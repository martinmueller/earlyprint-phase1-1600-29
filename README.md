# Early Seventeenth Century Texts
The EarlyPrint  texts in this repository experiment with the presentation of  enriched versions  of TCP transcriptions of Early 
Modern texts  from EEBO images.  Since 2000 the Text Creation Partnership (TCP) has been engaged in a project that can be 
roughly described as the creation  of a deduplicated library of printed texts in the English-speaking world before 1700.  The
 texts were transcribed from digital scans made by Early English Books Online (EEBO)  from microfilms images created over a
  sixty-year period beginning in the   thirties of the last century.  

A major goal of the EarlyPrint project is to create an accessible  diachronic corpus of Early Modern English. Its texts should be 
pleasant to read, computationally tractable on a corpus-wide basis, and suitable for collaboration curation and exploration
by amateur  and profesional end users from many walks of life. 

The EarlyPrint versions differ from their immediate TCP sources in several ways. To understand these difference it is useful
to reflect on the ways in which TCP texts relate to their sources. TCP texts are not documentary editions. Roughly speaking, 
a digital documentary edition of a text would record the material features of a "manifestation" of a particular "work", to use
FRBR terminology.  Given such an edition it should be possible to restore much of the look and feel of an old book without 
access to an image. 

TCP texts retain the pagination of the source texts, and they pay much attention to orthographic particulars. The texts were
encoded as SGML texts using the ASCII code. Characters and symbols outside this code were represented by  "character entities"
or periphrastic expressions describing characters that ASCII cannot represent directly. A "character entity" is bounded by an 
ampersand and a semicolon on the assumption that there is no word in any language that begins with the former and ends 
with the latter. "&amp;eacute;" is a character entity for an 'e' with an acute accent (é). "&abque;" is a character entity for a printer's 
abbreviation for the enclitic 'que' found at the end of many Latin words. And so on for well over 1000 character entities.

A TCP transcription puts all the words of a page on "the same" corresponding page, and it puts all the words from that page 
on its page in the same order. But page layout and typographical detail are largely ignored.   The transcriptions do not observe
line breaks but mark line-breaking hyphens where they appear. It uses the TEI <hi> element to mark cases where a word
or phrase differs typographically from its surrounding, but it tells you nothing about the typeface of the surrounding or
highlighted text. Text inside <hi> elements usually represents italics against surrounding Antiqua, but it could also be _Fraktur_
against Antiqua or Antiqua against surrounding _Fraktur_.
 
These observations are not offered as criticism but merely point out that, to use FRBR language, the TCP transcription are much more
loyal to the words of the "work" than to the material circumstances of the "manifestation" from which the words were transcribed.
The EarlyPrint texts share that loyalty to the words of the work, both in what they drop from and add to the TCP transcriptions. 

## What do EarlyPrint versions add?
The EarlyPrint versions are based on the TEI P5 versions  prepared by the late Sebastian Rahtz and available from the Oxford Text
Archive. They use a slightly modified version of the TEI simplePrint schema. Each text has been tokenized and linguistically
annotated with MorphAdorner. Every word is wrapped in a <w> element.  Lexical properties of each word, in particular the lemma,
the part of speech, and a standardized spelling are recorded as positional attributes of the <w> element.

Each word token is given a unique corpus-wide ID.  Because a diachronic perspective is critical to the EarlyPrint project, the text IDs
follow a chronological order. The TCP IDs, A or B followed by a five digit code, reflect processing order. We have developed a
four-digit alphabetical code arranging the TCP texts in chronological order (following the date information from the teiHeader)
and then mapping each text to a four-letter code in ascending order. The first letter maps to a decade, the following letters 
express sequence, though roughly. Here is an example of the first five TCP numbers in chronological order

* cadt	A00005	1515
* dafo	A00007	1528
* katv	A00001	1593
* nbkj	A00003	1623
* ncgx	A00002	1625

The four-letter codes are corpus-wide unique identifiers and are used as prefixes for the unique word ids. The current text
IDs--cadt-A00005, ncgx-A00002 etc.--are a convenient reminder that the EarlyPrint versions are different from, but closely
linked to, the TCP texts. In a search environment that returns word searches by a file ID, the default order of searches will always be
roughly chronological, which is useful.
 
The word IDs for each text concatenate the four-letter code with the EEBO image number and a simple word counter that
increments by 10 to allow for minor adjustments. EEBO image numbers are likely to remain the most stable identifiers for a
given image--somewhat like the Stephanus pages by which we have cited Plato for four centuries.

Consider _The Devil's Charter_, an early 17th century play (lcef-A04539).  The word 'Chronicler' appears on an unnumbered page
that is the source of the EEBO image and the early 20th century Tudor facsimile, now most readily available from the Internet
Archive. It appears on the left side of the EEBO double-page image 5. The EarlyPrint version assigns to it the ID lcef-005a-1940, which
with a little experience one translates into "look for that word in the lower middle of the left page."  That is as stable a
reference as you are likely to find in the universe of Early Modern print practices.

Not many people are interested in linguistic annotation per se. But most people find some use for some of it. Proper names are
an obvious example. The identification of passages in other languages is another. The EarlyPrint tag sections in Latin with a high
degree of accuracy.
 
##  Collaborative curation
Some features of the EarlyPrint version are designed to support collaborative curation. In the TCP texts lacunae are marked with
&lt;gap> elements. &lt;gap> elements are by definition empty: you cannot put anything in them. In the EarlyPrint version we substitute
a place holder elements that can be filled in. Thus 

		<gap unit='word' extent='2'/>
turns into 

	    <w>⟨◇⟩</w> <w>⟨◇⟩</w>
	
The TCP transcribers were instructed not to transcribe mathematical symbols, musical notation, or text in non-Roman alphabets.
The latter typically consists of short Greek or Hebrew fragments. EarlyPrint versions create place holder <seg> elements that can
be filled in witout disrupting the ID sequences.
 
Some EarlyPrint text have been--and many more will be--moved into a collaborative environment that lets users anywhere and
anytime suggest corrections for various textual defects. The environment currently hosts some 500 non-Shakespearean Early 
Modern plays at http://shc.earlyprint.org. Collaborative curation by three generations of undergraduates lowere the median
 defect rate per 10,000 words by an order of magnitude from 14 to 1.3.

The collaborative environment provides for editorial review of textual correction and their recording in a simple but formal 
apparatus criticus.

##  What do EarlyPrint versions drop?
When the TCP texts were first transcribed Unicode was still a novelty. Almost two decades later Unicode (and in particular UTF-8)
are standard equipment on computers and mobile devices. Moreover, tools for Natural Language Processing (NLP), which for many
years stuck to plain ASCII, can now handle text encoded in UTF-8.  The vast majority of character entity tokens are comfortably
and unambiguously represented by UTF-8 characters.

By far the most common UTF-8 character outside the ASCII set is tall 's'.  The EarlyPrint versions replace it. Tall 's' has no semantic
function and went out of the language in the course of the 18th century. If you think of a digital text as a static entity designed
mainly for reading, it is an easy character to display in UTF-8. It is, however, a very difficult character to work with in searching
 or collaborative work. Saying good bye to in a digital world makes good sense. 

The printer's abbreviations marked by chracter entities beginning with 'ab' have been silently resolves. Thus SGML 'cum&abque'
shows up as 'cumque' in the EarlyPrint version. Line-breaking hyphens have also been tacitly removed unless the word was
 clearly a hyphenated word. 

These are changes that cannot be rolled back. With regard to some other features, the content of a <w> element differs from the
TCP source, but the change is recorded in a rendition attribute and can be rolled back. The most common example is 'the', which in
 early texts often appears as a 'y' followed by a superscripted 'e'.  The 'y' in these cases was never the letter 'y' but  the printer's
  kludge for the 'thorn' character he didn't have. An EarlyPrint version of this phenomenon takes this form:
  
	<w lemma ="the'  pos = 'd' rendition="#sup-ye">the</w>
	
A particular implementation can follow or ignore the rendition attribute. 

The old Italian adage "traduttore traditore" (the translator is a traitor) also holds for digital surrogates. It is a matter of managing
Desdemona's "divided duties" with more skill and luck than she had.  EarlyPrint tries to combine loyalty to the words of the works
 with a commitment to making it possible for users to explore the affordances of digital surrogates even if they have limited technical
  skills and limited access to high-priced high-tech help. 
  
It is perhaps useful to think of a TCP text or this EarlyPrint version of it as the digital surrogate of an entity whose most
authoritative description is (or ought to be) found in the [English Short Title Catalog (ESTC)](http://estc.bl.uk). From that
perspective, the question of loyalty is not "How well does the EarlyPrint version follow its TCP source?" but "How well does 
the EarlyPrint version combine loyalty to a _work_ or _expression_ described in the ESTC with attention to the technical 
and social conditions of the  21st century environment in which that work will circulate?"
  
## How are the texts organized in the repository?

The texts are group in subdirectories with two-letter codes. The initial letter 'a' is used for texts before 1500. There are not very
many of them, and some of them are the first print editions of much earlier texts by Chaucer, Lydgate, and Mallory.  The letters
 'b' to 'u' mark decades of the sixteenth and seventeenth centuries.  The second letter is generated by the progression of dates
  in the teiHeader files.  It is a useful chronological pointer, but to be taken with a grain of salt. 

## The teiHeader sections
As of now (May 2017), the header elements for each text replicate the TCP header. Developing a proper EarlyPrint header is high
on the to-do list. 

## What is in this repository?
This repository contains 2,400 of 2787 texts of texts between 1600 and 1629 in Phase 1 of the TCP project. All Phase 1 texts
are in the public domain and may be used by anybody in any way for any non-commercial or commercial project. The  texts
not included fall into several categories. Some are not English. Others created various processing problems that we need to
solve before adding them to the repository. In addition texts more than 30MB in length were excluded because of size limitations 
in the repository.