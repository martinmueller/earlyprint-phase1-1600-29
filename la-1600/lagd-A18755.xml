<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>A doble almanacke or kalender drawne for this present yeere 1600, which is leape yeere and from the beginning of the worlde 5562 the first kalender seruing generally for all England, and the other necessarie for such as shal haue occasion of traffique beyond the seas for their needefull busines / collected and gathered for the former vse by Robert Watson ...</title>
        <author>Watson, Robert, fl. 1581-1605.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1600</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A18755</idno>
        <idno type="STC">STC 525.4</idno>
        <idno type="STC">ESTC S2474</idno>
        <idno type="EEBO-CITATION">24228711</idno>
        <idno type="OCLC">ocm 24228711</idno>
        <idno type="VID">27401</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A18755)</note>
        <note>Transcribed from: (Early English Books Online ; image set 27401)</note>
        <note>Images scanned from microfilm: (Early English books, 1475-1640 ; 1846:25)</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>A doble almanacke or kalender drawne for this present yeere 1600, which is leape yeere and from the beginning of the worlde 5562 the first kalender seruing generally for all England, and the other necessarie for such as shal haue occasion of traffique beyond the seas for their needefull busines / collected and gathered for the former vse by Robert Watson ...</title>
            <author>Watson, Robert, fl. 1581-1605.</author>
          </titleStmt>
          <extent>[1+] p.</extent>
          <publicationStmt>
            <publisher>By Richard Watkins and Iames Robertes,</publisher>
            <pubPlace>Imprinted at London :</pubPlace>
            <date>[1600]</date>
          </publicationStmt>
          <notesStmt>
            <note>Title within illustrated border.</note>
            <note>Imperfect: lacks all after t.p.</note>
            <note>Reproduction of original in the Cambridge University Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Almanacs, English.</term>
          <term>Ephemerides.</term>
          <term>Astrology -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-08</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-08</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-09</date><label>Elspeth Healey</label>
        Sampled and proofread
      </change>
      <change><date>2007-09</date><label>Elspeth Healey</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="lagd-t">
    <body xml:id="lagd-e0">
      <div type="document" xml:id="lagd-e10">
        <pb facs="tcp:27401:1" xml:id="lagd-001-a"/>
        <p xml:id="lagd-e20">
          <figure xml:id="lagd-e30">
            <p xml:id="lagd-e40">
              <w xml:id="lagd-001-a-0010" lemma="〈…〉" reg="〈…〉" pos="#zz">〈…〉</w>
              <w lemma="or" pos="cc" reg="or" xml:id="lagd-001-a-0020">or</w>
              <w lemma="calendar" pos="n1" reg="Calendar" xml:id="lagd-001-a-0030">Kalender</w>
              <w lemma="draw" pos="vvn" reg="drawn" xml:id="lagd-001-a-0040">drawne</w>
              <w lemma="for" pos="acp-p" reg="for" xml:id="lagd-001-a-0050">for</w>
              <w lemma="which" pos="crq-r" reg="Which" xml:id="lagd-001-a-0060">Which</w>
              <w lemma="be" pos="vvz" reg="is" xml:id="lagd-001-a-0070">is</w>
              <w lemma="leap" pos="vvi" reg="leap" xml:id="lagd-001-a-0080">leape</w>
              <w lemma="year" pos="n1" reg="year" xml:id="lagd-001-a-0090">yeere</w>
              <pc xml:id="lagd-001-a-0100">,</pc>
              <w lemma="and" pos="cc" reg="and" xml:id="lagd-001-a-0110">and</w>
              <w lemma="from" pos="acp-p" reg="from" xml:id="lagd-001-a-0120">from</w>
              <w lemma="the" pos="d" reg="the" xml:id="lagd-001-a-0130">the</w>
              <w lemma="begin" pos="n1_vg" reg="beginning" xml:id="lagd-001-a-0140">beginning</w>
              <w lemma="of" pos="acp-p" reg="of" xml:id="lagd-001-a-0150">of</w>
              <w lemma="the" pos="d" reg="the" xml:id="lagd-001-a-0160">the</w>
              <w lemma="world" pos="n1" reg="world" xml:id="lagd-001-a-0170">worlde</w>
              <pc unit="sentence" xml:id="lagd-001-a-0180">.</pc>
              <w lemma="5562." pos="crd" reg="5562." xml:id="lagd-001-a-0190">5562.</w>
              <w lemma="general" pos="av_j" reg="generally" xml:id="lagd-001-a-0200">generally</w>
              <w lemma="for" pos="acp-p" reg="for" xml:id="lagd-001-a-0210">for</w>
              <w lemma="all" pos="d" reg="all" xml:id="lagd-001-a-0220">all</w>
              <w lemma="England" pos="n1-nn" reg="England" xml:id="lagd-001-a-0230">England</w>
              <pc xml:id="lagd-001-a-0240">,</pc>
              <w lemma="and" pos="cc" reg="and" xml:id="lagd-001-a-0250">and</w>
              <w lemma="as" pos="acp-cs" reg="as" xml:id="lagd-001-a-0260">as</w>
              <w lemma="shall" pos="vmb" reg="shall" xml:id="lagd-001-a-0270">shal</w>
              <w lemma="have" pos="vvi" reg="have" xml:id="lagd-001-a-0280">haue</w>
              <w lemma="occasion" pos="n1" reg="occasion" xml:id="lagd-001-a-0290">occasion</w>
              <w lemma="of" pos="acp-p" reg="of" xml:id="lagd-001-a-0300">of</w>
              <w lemma="traf" pos="n1" reg="traf" xml:id="lagd-001-a-0310">traf</w>
              <w xml:id="lagd-001-a-0320" lemma="〈…〉" reg="〈…〉" pos="#zz">〈…〉</w>
              <w lemma="needful" pos="j" reg="needful" xml:id="lagd-001-a-0330">needefull</w>
              <w lemma="business" pos="n1" reg="business" xml:id="lagd-001-a-0340">busines</w>
              <pc xml:id="lagd-001-a-0350">.</pc>
              <w lemma="former" pos="j" reg="former" xml:id="lagd-001-a-0360">former</w>
              <w lemma="use" pos="n1" reg="use" xml:id="lagd-001-a-0370">vse</w>
              <w lemma="by" pos="acp-p" reg="by" xml:id="lagd-001-a-0380">by</w>
              <w lemma="Robert" pos="n1-nn" reg="Robert" xml:id="lagd-001-a-0390">Robert</w>
              <w lemma="watson" pos="n1-nn" reg="Watson" xml:id="lagd-001-a-0400">Watson</w>
              <w lemma="practioner" pos="n1" reg="practioner" xml:id="lagd-001-a-0410">practioner</w>
              <w lemma="of" pos="acp-p" reg="of" xml:id="lagd-001-a-0420">of</w>
              <w lemma="physic" pos="n1" reg="Physic" xml:id="lagd-001-a-0430">Phisicke</w>
              <w lemma="in" pos="acp-p" reg="in" xml:id="lagd-001-a-0440">in</w>
              <w lemma="the" pos="d" reg="the" xml:id="lagd-001-a-0450">the</w>
              <w lemma="town" pos="n1" reg="Town" xml:id="lagd-001-a-0460">Towne</w>
              <w lemma="of" pos="acp-p" reg="of" xml:id="lagd-001-a-0470">of</w>
              <w lemma="brancktry" pos="n1" reg="Brancktry" xml:id="lagd-001-a-0480">Brancktry</w>
              <pc unit="sentence" xml:id="lagd-001-a-0490">.</pc>
            </p>
            <p xml:id="lagd-e50">
              <w lemma="quod" pos="fw-la" reg="Quod" xml:id="lagd-001-a-0500">Quod</w>
              <w lemma="gratis" pos="av" reg="gratis" xml:id="lagd-001-a-0510">gratis</w>
              <w lemma="grate" pos="n1" reg="grate" xml:id="lagd-001-a-0520">grate</w>
              <pc unit="sentence" xml:id="lagd-001-a-0530">.</pc>
            </p>
            <p xml:id="lagd-e60">
              <w lemma="●●chard" pos="n1" reg="●●chard" xml:id="lagd-001-a-0540">●●chard</w>
              <w lemma="watkins" pos="n1-nn" reg="Watkins" xml:id="lagd-001-a-0550">Watkins</w>
              <w lemma="and" pos="cc" reg="and" xml:id="lagd-001-a-0560">and</w>
              <w lemma="james" pos="n1-nn" reg="james" xml:id="lagd-001-a-0570">Iames</w>
              <hi xml:id="lagd-e70">
                <w lemma="roberte" pos="n2" reg="Robertes" xml:id="lagd-001-a-0580">Robertes</w>
                <pc unit="sentence" xml:id="lagd-001-a-0590">.</pc>
              </hi>
            </p>
          </figure>
        </p>
      </div>
    </body>
    <back xml:id="lagd-t-b"/>
  </text>
</TEI>

