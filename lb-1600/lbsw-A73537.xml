<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Be it knowne vnto all men, that I Nicholas Bowden chirurgion, cutter of the stone, and also occultest, curer of the ruptures without cutting, with the helpe of almightie God, can cure and helpe these sicknesses and infirmities following</title>
        <author>Bowden, Nicholas, fl. 1605?</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1605</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-10 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A73537</idno>
        <idno type="STC">STC 3432.3</idno>
        <idno type="STC">ESTC S124644</idno>
        <idno type="EEBO-CITATION">99900321</idno>
        <idno type="PROQUEST">99900321</idno>
        <idno type="VID">150801</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A73537)</note>
        <note>Transcribed from: (Early English Books Online ; image set 150801)</note>
        <note>Images scanned from microfilm: (Early English books, 1475-1640 ; 1982:6)</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Be it knowne vnto all men, that I Nicholas Bowden chirurgion, cutter of the stone, and also occultest, curer of the ruptures without cutting, with the helpe of almightie God, can cure and helpe these sicknesses and infirmities following</title>
            <author>Bowden, Nicholas, fl. 1605?</author>
          </titleStmt>
          <extent>1 sheet ([1] p.)</extent>
          <publicationStmt>
            <publisher>J. Roberts,</publisher>
            <pubPlace>[London :</pubPlace>
            <date>1605?]</date>
          </publicationStmt>
          <notesStmt>
            <note>Claims cures for kidney stone, hare lip, etc.</note>
            <note>Imprint from STC.</note>
            <note>Reproduction of original in the Bodleian Library, Oxford, England.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Medicine, Popular -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-08</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-11</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2009-01</date><label>John Pas</label>
        Sampled and proofread
      </change>
      <change><date>2009-01</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="lbsw-t">
    <body xml:id="lbsw-e0">
      <div type="advert_for_surgeon" xml:id="lbsw-e10">
        <pb facs="tcp:150801:1" xml:id="lbsw-001-a"/>
        <p xml:id="lbsw-e20">
          <w lemma="be" pos="vvb" reg="BE" rendition="#decorinit" xml:id="lbsw-001-a-0010">BE</w>
          <w lemma="it" pos="pn" reg="it" xml:id="lbsw-001-a-0020">it</w>
          <w lemma="know" pos="vvn" reg="known" xml:id="lbsw-001-a-0030">knowne</w>
          <w lemma="unto" pos="acp-p" reg="unto" xml:id="lbsw-001-a-0040">vnto</w>
          <w lemma="all" pos="d" reg="all" xml:id="lbsw-001-a-0050">all</w>
          <w lemma="man" pos="n2" reg="men" xml:id="lbsw-001-a-0060">men</w>
          <pc xml:id="lbsw-001-a-0070">,</pc>
          <w lemma="that" pos="cs" reg="that" xml:id="lbsw-001-a-0080">that</w>
          <w lemma="i" pos="pns" reg="I" xml:id="lbsw-001-a-0090">I</w>
          <hi xml:id="lbsw-e30">
            <w lemma="Nicholas" pos="n1-nn" reg="Nicholas" xml:id="lbsw-001-a-0100">Nicholas</w>
            <w lemma="bowden" pos="n1-nn" reg="Bowden" xml:id="lbsw-001-a-0110">Bowden</w>
          </hi>
          <w lemma="chirurgeon" pos="n1" reg="Chirurgeon" xml:id="lbsw-001-a-0120">Chirurgion</w>
          <pc xml:id="lbsw-001-a-0130">,</pc>
          <w lemma="cutter" pos="n1" reg="cutter" xml:id="lbsw-001-a-0140">cutter</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="lbsw-001-a-0150">of</w>
          <w lemma="the" pos="d" reg="the" xml:id="lbsw-001-a-0160">the</w>
          <w lemma="stone" pos="n1" reg="stone" xml:id="lbsw-001-a-0170">stone</w>
          <pc xml:id="lbsw-001-a-0180">,</pc>
          <w lemma="and" pos="cc" reg="and" xml:id="lbsw-001-a-0190">and</w>
          <w lemma="also" pos="av" reg="also" xml:id="lbsw-001-a-0200">also</w>
          <w lemma="occule" pos="j-s" reg="Occulest" xml:id="lbsw-001-a-0210">Occulest</w>
          <pc xml:id="lbsw-001-a-0220">,</pc>
          <w lemma="curer" pos="n1" reg="curer" xml:id="lbsw-001-a-0230">curer</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="lbsw-001-a-0240">of</w>
          <w lemma="the" pos="d" reg="the" xml:id="lbsw-001-a-0250">the</w>
          <w lemma="rupture" pos="n2" reg="Ruptures" xml:id="lbsw-001-a-0260">Ruptures</w>
          <w lemma="without" pos="acp-p" reg="without" xml:id="lbsw-001-a-0270">without</w>
          <w lemma="cut" pos="vvg" reg="cutting" xml:id="lbsw-001-a-0280">cutting</w>
          <pc xml:id="lbsw-001-a-0290">,</pc>
          <w lemma="with" pos="acp-p" reg="with" xml:id="lbsw-001-a-0300">with</w>
          <w lemma="the" pos="d" reg="the" xml:id="lbsw-001-a-0310">the</w>
          <w lemma="help" pos="n1" reg="help" xml:id="lbsw-001-a-0320">helpe</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="lbsw-001-a-0330">of</w>
          <w lemma="almighty" pos="j" reg="almighty" xml:id="lbsw-001-a-0340">almightie</w>
          <w lemma="God" pos="n1-nn" reg="God" xml:id="lbsw-001-a-0350">God</w>
          <pc xml:id="lbsw-001-a-0360">,</pc>
          <w lemma="can" pos="vmb" reg="can" xml:id="lbsw-001-a-0370">can</w>
          <w lemma="cure" pos="vvi" reg="cure" xml:id="lbsw-001-a-0380">cure</w>
          <w lemma="and" pos="cc" reg="and" xml:id="lbsw-001-a-0390">and</w>
          <w lemma="help" pos="vvi" reg="help" xml:id="lbsw-001-a-0400">helpe</w>
          <w lemma="these" pos="d" reg="these" xml:id="lbsw-001-a-0410">these</w>
          <w lemma="sickness" pos="n2" reg="sicknesses" xml:id="lbsw-001-a-0420">sicknesses</w>
          <w lemma="and" pos="cc" reg="and" xml:id="lbsw-001-a-0430">and</w>
          <w lemma="infirmity" pos="n2" reg="infirmities" xml:id="lbsw-001-a-0440">infirmities</w>
          <w lemma="follow" pos="vvg" reg="following" xml:id="lbsw-001-a-0450">following</w>
          <pc unit="sentence" xml:id="lbsw-001-a-0460">.</pc>
        </p>
        <list xml:id="lbsw-e40">
          <item xml:id="lbsw-e50">
            <w lemma="in" pos="acp-p" reg="In" xml:id="lbsw-001-a-0470">In</w>
            <w lemma="primis" pos="fw-la" reg="primis" xml:id="lbsw-001-a-0480">primis</w>
            <pc xml:id="lbsw-001-a-0490">,</pc>
            <w lemma="i" pos="pns" reg="I" xml:id="lbsw-001-a-0500">I</w>
            <w lemma="can" pos="vmb" reg="can" xml:id="lbsw-001-a-0510">can</w>
            <w lemma="by" pos="acp-p" reg="by" xml:id="lbsw-001-a-0520">by</w>
            <w lemma="cut" pos="vvg" reg="cutting" xml:id="lbsw-001-a-0530">cutting</w>
            <w lemma="bring" pos="vvi" reg="bring" xml:id="lbsw-001-a-0540">bring</w>
            <w lemma="to" pos="acp-p" reg="to" xml:id="lbsw-001-a-0550">to</w>
            <w lemma="health" pos="n1" reg="health" xml:id="lbsw-001-a-0560">health</w>
            <pc xml:id="lbsw-001-a-0570">,</pc>
            <w lemma="be" pos="vvb" reg="be" xml:id="lbsw-001-a-0580">be</w>
            <w lemma="they" pos="pns" reg="they" xml:id="lbsw-001-a-0590">they</w>
            <w lemma="man" pos="n2" reg="men" xml:id="lbsw-001-a-0600">men</w>
            <w lemma="or" pos="cc" reg="or" xml:id="lbsw-001-a-0610">or</w>
            <w lemma="little" pos="j" reg="little" xml:id="lbsw-001-a-0620">little</w>
            <w lemma="child" pos="n2" reg="children" xml:id="lbsw-001-a-0630">children</w>
            <pc xml:id="lbsw-001-a-0640">,</pc>
            <w lemma="that" pos="cs" reg="that" xml:id="lbsw-001-a-0650">that</w>
            <w lemma="have" pos="vvb" reg="have" xml:id="lbsw-001-a-0660">haue</w>
            <w lemma="the" pos="d" reg="the" xml:id="lbsw-001-a-0670">the</w>
            <w lemma="confirm" pos="j_vn" reg="confirmed" xml:id="lbsw-001-a-0680">confirmed</w>
            <w lemma="stone" pos="n1" reg="stone" xml:id="lbsw-001-a-0690">stone</w>
            <w lemma="in" pos="acp-p" reg="in" xml:id="lbsw-001-a-0700">in</w>
            <w lemma="the" pos="d" reg="the" xml:id="lbsw-001-a-0710">the</w>
            <w lemma="bladder" pos="n1" reg="bladder" xml:id="lbsw-001-a-0720">bladder</w>
            <pc xml:id="lbsw-001-a-0730">,</pc>
            <w lemma="i" pos="pns" reg="I" xml:id="lbsw-001-a-0740">I</w>
            <w lemma="can" pos="vmb" reg="can" xml:id="lbsw-001-a-0750">can</w>
            <w lemma="take" pos="vvi" reg="take" xml:id="lbsw-001-a-0760">take</w>
            <w lemma="the" pos="d" reg="the" xml:id="lbsw-001-a-0770">the</w>
            <w lemma="same" pos="d" reg="same" xml:id="lbsw-001-a-0780">same</w>
            <w lemma="away" pos="av" reg="away" xml:id="lbsw-001-a-0790">away</w>
            <w lemma="from" pos="acp-p" reg="from" xml:id="lbsw-001-a-0800">from</w>
            <w lemma="woman" pos="n2" reg="women" xml:id="lbsw-001-a-0810">women</w>
            <w lemma="or" pos="cc" reg="or" xml:id="lbsw-001-a-0820">or</w>
            <w lemma="girl" pos="n2" reg="girls" xml:id="lbsw-001-a-0830">girles</w>
            <w lemma="without" pos="acp-p" reg="without" xml:id="lbsw-001-a-0840">without</w>
            <w lemma="cut" pos="vvg" reg="cutting" xml:id="lbsw-001-a-0850">cutting</w>
            <pc unit="sentence" xml:id="lbsw-001-a-0860">.</pc>
          </item>
          <item xml:id="lbsw-e60">
            <w lemma="also" pos="av" reg="Also" xml:id="lbsw-001-a-0870">Also</w>
            <w lemma="i" pos="pns" reg="I" xml:id="lbsw-001-a-0880">I</w>
            <w lemma="can" pos="vmb" reg="can" xml:id="lbsw-001-a-0890">can</w>
            <w lemma="help" pos="vvi" reg="help" xml:id="lbsw-001-a-0900">helpe</w>
            <w lemma="those" pos="d" reg="those" xml:id="lbsw-001-a-0910">those</w>
            <w lemma="that" pos="cs" reg="that" xml:id="lbsw-001-a-0920">that</w>
            <w lemma="be" pos="vvb" reg="are" xml:id="lbsw-001-a-0930">are</w>
            <w lemma="trouble" pos="vvn" reg="troubled" xml:id="lbsw-001-a-0940">troubled</w>
            <w lemma="with" pos="acp-p" reg="with" xml:id="lbsw-001-a-0950">with</w>
            <w lemma="the" pos="d" reg="the" xml:id="lbsw-001-a-0960">the</w>
            <w lemma="gravel" pos="n1" reg="gravel" xml:id="lbsw-001-a-0970">grauell</w>
            <w lemma="in" pos="acp-p" reg="in" xml:id="lbsw-001-a-0980">in</w>
            <w lemma="the" pos="d" reg="the" xml:id="lbsw-001-a-0990">the</w>
            <w lemma="kidney" pos="n2" reg="Kidneys" xml:id="lbsw-001-a-1000">Kidneyes</w>
            <w lemma="or" pos="cc" reg="or" xml:id="lbsw-001-a-1010">or</w>
            <w lemma="rein" pos="n2" reg="Reins" xml:id="lbsw-001-a-1020">Reines</w>
            <w lemma="of" pos="acp-p" reg="of" xml:id="lbsw-001-a-1030">of</w>
            <w lemma="the" pos="d" reg="the" xml:id="lbsw-001-a-1040">the</w>
            <w lemma="back" pos="n1" reg="back" xml:id="lbsw-001-a-1050">backe</w>
            <pc xml:id="lbsw-001-a-1060">,</pc>
            <w lemma="also" pos="av" reg="also" xml:id="lbsw-001-a-1070">also</w>
            <w lemma="vlceration" pos="n2" reg="Vlcerations" xml:id="lbsw-001-a-1080">Vlcerations</w>
            <pc xml:id="lbsw-001-a-1090">,</pc>
            <w lemma="apostumation" pos="n2" reg="Apostumations" xml:id="lbsw-001-a-1100">Apostumations</w>
            <pc xml:id="lbsw-001-a-1110">,</pc>
            <w lemma="or" pos="cc" reg="or" xml:id="lbsw-001-a-1120">or</w>
            <w lemma="other" pos="d" reg="other" xml:id="lbsw-001-a-1130">other</w>
            <w lemma="impediment" pos="n2" reg="impediments" xml:id="lbsw-001-a-1140">impediments</w>
            <w lemma="in" pos="acp-p" reg="in" xml:id="lbsw-001-a-1150">in</w>
            <w lemma="the" pos="d" reg="the" xml:id="lbsw-001-a-1160">the</w>
            <w lemma="place" pos="n2" reg="places" xml:id="lbsw-001-a-1170">places</w>
            <w lemma="aforesaid" pos="j" reg="aforesaid" xml:id="lbsw-001-a-1180">afore-said</w>
            <pc unit="sentence" xml:id="lbsw-001-a-1190">.</pc>
          </item>
          <item xml:id="lbsw-e70">
            <w lemma="i" pos="pns" reg="I" xml:id="lbsw-001-a-1200">I</w>
            <w lemma="can" pos="vmb" reg="can" xml:id="lbsw-001-a-1210">can</w>
            <w lemma="without" pos="acp-p" reg="without" xml:id="lbsw-001-a-1220">without</w>
            <w lemma="cut" pos="j_vg" reg="cutting" xml:id="lbsw-001-a-1230">cutting</w>
            <w lemma="cure" pos="n1" reg="cure" xml:id="lbsw-001-a-1240">cure</w>
            <w lemma="all" pos="d" reg="all" xml:id="lbsw-001-a-1250">all</w>
            <w lemma="rupture" pos="n2" reg="Ruptures" xml:id="lbsw-001-a-1260">Ruptures</w>
            <w lemma="or" pos="cc" reg="or" xml:id="lbsw-001-a-1270">or</w>
            <w lemma="burstinge" pos="n2" reg="Burstinges" xml:id="lbsw-001-a-1280">Burstinges</w>
            <pc xml:id="lbsw-001-a-1290">,</pc>
            <w lemma="as" pos="acp-cs" reg="as" xml:id="lbsw-001-a-1300">as</w>
            <w lemma="namely" pos="av" reg="namely" xml:id="lbsw-001-a-1310">namely</w>
            <pc xml:id="lbsw-001-a-1320">,</pc>
            <hi xml:id="lbsw-e80">
              <w lemma="ramex" pos="fw-la" reg="Ramex" xml:id="lbsw-001-a-1330">Ramex</w>
              <pc xml:id="lbsw-001-a-1340">,</pc>
              <w lemma="inguinales" pos="fw-la" reg="Inguinales" xml:id="lbsw-001-a-1350">Inguinales</w>
              <pc xml:id="lbsw-001-a-1360">,</pc>
              <w lemma="intestenales" pos="fw-la" reg="Intestenales" xml:id="lbsw-001-a-1370">Intestenales</w>
              <pc xml:id="lbsw-001-a-1380">,</pc>
              <w lemma="Serbales" pos="n1-nn" reg="Serbales" xml:id="lbsw-001-a-1390">Serbales</w>
              <pc unit="sentence" xml:id="lbsw-001-a-1400">.</pc>
            </hi>
          </item>
          <item xml:id="lbsw-e90">
            <w lemma="i" pos="pns" reg="I" xml:id="lbsw-001-a-1410">I</w>
            <w lemma="can" pos="vmb" reg="can" xml:id="lbsw-001-a-1420">can</w>
            <w lemma="also" pos="av" reg="also" xml:id="lbsw-001-a-1430">also</w>
            <w lemma="cure" pos="vvi" reg="cure" xml:id="lbsw-001-a-1440">cure</w>
            <hi xml:id="lbsw-e100">
              <w lemma="herniam" pos="fw-la" reg="Herniam" xml:id="lbsw-001-a-1450">Herniam</w>
              <pc xml:id="lbsw-001-a-1460">,</pc>
              <w lemma="humoralem" pos="fw-la" reg="Humoralem" xml:id="lbsw-001-a-1470">Humoralem</w>
              <pc xml:id="lbsw-001-a-1480">,</pc>
              <w lemma="aquosam" pos="fw-la" reg="Aquosam" xml:id="lbsw-001-a-1490">Aquosam</w>
              <pc xml:id="lbsw-001-a-1500">,</pc>
              <w lemma="ventosam" pos="fw-la" reg="Ventosam" xml:id="lbsw-001-a-1510">Ventosam</w>
              <pc xml:id="lbsw-001-a-1520">,</pc>
              <w lemma="carnosam" pos="fw-la" reg="Carnosam" xml:id="lbsw-001-a-1530">Carnosam</w>
              <pc xml:id="lbsw-001-a-1540">,</pc>
              <w lemma="verrucosam" pos="fw-la" reg="Verrucosam" xml:id="lbsw-001-a-1550">Verrucosam</w>
              <pc xml:id="lbsw-001-a-1560">,</pc>
            </hi>
            <w lemma="some" pos="d" reg="some" xml:id="lbsw-001-a-1570">some</w>
            <w lemma="by" pos="acp-p" reg="by" xml:id="lbsw-001-a-1580">by</w>
            <w lemma="cut" pos="vvg" reg="cutting" xml:id="lbsw-001-a-1590">cutting</w>
            <pc xml:id="lbsw-001-a-1600">,</pc>
            <w lemma="and" pos="cc" reg="and" xml:id="lbsw-001-a-1610">and</w>
            <w lemma="some" pos="d" reg="some" xml:id="lbsw-001-a-1620">some</w>
            <w lemma="without" pos="acp-p" reg="without" xml:id="lbsw-001-a-1630">without</w>
            <pc xml:id="lbsw-001-a-1640">,</pc>
            <w lemma="accord" pos="vvg" reg="according" xml:id="lbsw-001-a-1650">according</w>
            <w lemma="to" pos="acp-p" reg="to" xml:id="lbsw-001-a-1660">to</w>
            <w lemma="the" pos="d" reg="the" xml:id="lbsw-001-a-1670">the</w>
            <w lemma="disposition" pos="n1" reg="disposition" xml:id="lbsw-001-a-1680">disposition</w>
            <w lemma="of" pos="acp-p" reg="of" xml:id="lbsw-001-a-1690">of</w>
            <w lemma="the" pos="d" reg="the" xml:id="lbsw-001-a-1700">the</w>
            <w lemma="accident" pos="n1" reg="accident" xml:id="lbsw-001-a-1710">accident</w>
            <pc unit="sentence" xml:id="lbsw-001-a-1720">.</pc>
          </item>
          <item xml:id="lbsw-e110">
            <w lemma="i" pos="pns" reg="I" xml:id="lbsw-001-a-1730">I</w>
            <w lemma="also" pos="av" reg="also" xml:id="lbsw-001-a-1740">also</w>
            <w lemma="cure" pos="vvi" reg="cure" xml:id="lbsw-001-a-1750">cure</w>
            <w lemma="wry" pos="j" reg="wry" xml:id="lbsw-001-a-1760">wrie</w>
            <w lemma="neck" pos="n2" reg="necks" xml:id="lbsw-001-a-1770">necks</w>
            <pc xml:id="lbsw-001-a-1780">,</pc>
            <w lemma="wry" pos="j" reg="wry" xml:id="lbsw-001-a-1790">wrie</w>
            <w lemma="leg" pos="n2" reg="legs" xml:id="lbsw-001-a-1800">legges</w>
            <pc xml:id="lbsw-001-a-1810">,</pc>
            <w lemma="and" pos="cc" reg="and" xml:id="lbsw-001-a-1820">and</w>
            <w lemma="crooked" pos="j" reg="crooked" xml:id="lbsw-001-a-1830">crooked</w>
            <w lemma="body" pos="n2" reg="bodies" xml:id="lbsw-001-a-1840">bodies</w>
            <pc xml:id="lbsw-001-a-1850">,</pc>
            <w lemma="by" pos="acp-p" reg="by" xml:id="lbsw-001-a-1860">by</w>
            <w lemma="a" pos="d" reg="a" xml:id="lbsw-001-a-1870">a</w>
            <w lemma="rare" pos="j" reg="rare" xml:id="lbsw-001-a-1880">rare</w>
            <w lemma="mean" pos="n2" reg="means" xml:id="lbsw-001-a-1890">meanes</w>
            <w lemma="new" pos="av_j" reg="newly" xml:id="lbsw-001-a-1900">newly</w>
            <w lemma="practise" pos="vvn" reg="practised" xml:id="lbsw-001-a-1910">practised</w>
            <pc unit="sentence" xml:id="lbsw-001-a-1920">.</pc>
          </item>
          <item xml:id="lbsw-e120">
            <w lemma="all" pos="av_d" reg="All" xml:id="lbsw-001-a-1930">All</w>
            <w lemma="rume" pos="n2" reg="Rumes" xml:id="lbsw-001-a-1940">Rumes</w>
            <pc xml:id="lbsw-001-a-1950">,</pc>
            <w lemma="pearl" pos="n2" reg="pearls" xml:id="lbsw-001-a-1960">pearles</w>
            <pc xml:id="lbsw-001-a-1970">,</pc>
            <w lemma="blemish" pos="n2" reg="blemishes" xml:id="lbsw-001-a-1980">blemishes</w>
            <pc xml:id="lbsw-001-a-1990">,</pc>
            <w lemma="or" pos="cc" reg="or" xml:id="lbsw-001-a-2000">or</w>
            <w lemma="catteract" pos="vvz" reg="Catteracts" xml:id="lbsw-001-a-2010">Catteracts</w>
            <w lemma="curable" pos="j" reg="curable" xml:id="lbsw-001-a-2020">curable</w>
            <pc xml:id="lbsw-001-a-2030">,</pc>
            <w lemma="although" pos="cs" reg="although" xml:id="lbsw-001-a-2040">although</w>
            <w lemma="they" pos="pns" reg="they" xml:id="lbsw-001-a-2050">they</w>
            <w lemma="have" pos="vvb" reg="have" xml:id="lbsw-001-a-2060">haue</w>
            <w lemma="be" pos="vvn" reg="been" xml:id="lbsw-001-a-2070">beene</w>
            <w lemma="long" pos="av_j" reg="long" xml:id="lbsw-001-a-2080">long</w>
            <w lemma="blind" pos="j" reg="blind" xml:id="lbsw-001-a-2090">blinde</w>
            <pc xml:id="lbsw-001-a-2100">,</pc>
            <w lemma="they" pos="pns" reg="they" xml:id="lbsw-001-a-2110">they</w>
            <w lemma="shall" pos="vmb" reg="shall" xml:id="lbsw-001-a-2120">shall</w>
            <w lemma="in" pos="acp-p" reg="in" xml:id="lbsw-001-a-2130">in</w>
            <w lemma="short" pos="j" reg="short" xml:id="lbsw-001-a-2140">short</w>
            <w lemma="time" pos="n1" reg="time" xml:id="lbsw-001-a-2150">time</w>
            <w lemma="receive" pos="vvi" reg="receive" xml:id="lbsw-001-a-2160">receaue</w>
            <w lemma="sight" pos="n1" reg="sight" xml:id="lbsw-001-a-2170">sight</w>
            <pc unit="sentence" xml:id="lbsw-001-a-2180">.</pc>
          </item>
          <item xml:id="lbsw-e130">
            <w lemma="all" pos="av_d" reg="All" xml:id="lbsw-001-a-2190">All</w>
            <w lemma="hare" pos="n1" reg="hare" xml:id="lbsw-001-a-2200">hare</w>
            <w lemma="or" pos="cc" reg="or" xml:id="lbsw-001-a-2210">or</w>
            <w lemma="cleave" pos="j_vn" reg="cloven" xml:id="lbsw-001-a-2220">cleft</w>
            <w lemma="lip" pos="n2" reg="lips" xml:id="lbsw-001-a-2230">lippes</w>
            <pc xml:id="lbsw-001-a-2240">,</pc>
            <w lemma="i" pos="pns" reg="I" xml:id="lbsw-001-a-2250">I</w>
            <w lemma="cure" pos="vvb" reg="cure" xml:id="lbsw-001-a-2260">cure</w>
            <w lemma="in" pos="acp-p" reg="in" xml:id="lbsw-001-a-2270">in</w>
            <w lemma="short" pos="j" reg="short" xml:id="lbsw-001-a-2280">short</w>
            <w lemma="time" pos="n1" reg="time" xml:id="lbsw-001-a-2290">time</w>
            <pc unit="sentence" xml:id="lbsw-001-a-2300">.</pc>
          </item>
          <item xml:id="lbsw-e140">
            <w lemma="all" pos="av_d" reg="All" xml:id="lbsw-001-a-2310">All</w>
            <w lemma="excressention" pos="n2" reg="Excressentions" xml:id="lbsw-001-a-2320">Excressentions</w>
            <w lemma="or" pos="cc" reg="or" xml:id="lbsw-001-a-2330">or</w>
            <w lemma="wen" pos="n2" reg="Wens" xml:id="lbsw-001-a-2340">Wennes</w>
            <pc xml:id="lbsw-001-a-2350">,</pc>
            <w lemma="in" pos="acp-p" reg="in" xml:id="lbsw-001-a-2360">in</w>
            <w lemma="what" pos="crq-r" reg="what" xml:id="lbsw-001-a-2370">what</w>
            <w lemma="place" pos="n1" reg="place" xml:id="lbsw-001-a-2380">place</w>
            <w lemma="so" pos="av" reg="so" xml:id="lbsw-001-a-2390">so</w>
            <w lemma="ever" pos="av" reg="ever" xml:id="lbsw-001-a-2400">euer</w>
            <pc xml:id="lbsw-001-a-2410">,</pc>
            <w lemma="i" pos="pns" reg="I" xml:id="lbsw-001-a-2420">I</w>
            <w lemma="can" pos="vmb" reg="can" xml:id="lbsw-001-a-2430">can</w>
            <w lemma="cure" pos="vvi" reg="cure" xml:id="lbsw-001-a-2440">cure</w>
            <w lemma="they" pos="pno" reg="them" xml:id="lbsw-001-a-2450">them</w>
            <pc unit="sentence" xml:id="lbsw-001-a-2460">.</pc>
          </item>
          <item xml:id="lbsw-e150">
            <w lemma="those" pos="d" reg="Those" xml:id="lbsw-001-a-2470">Those</w>
            <w lemma="woman" pos="n2" reg="Women" xml:id="lbsw-001-a-2480">Women</w>
            <w lemma="that" pos="cs" reg="that" xml:id="lbsw-001-a-2490">that</w>
            <w lemma="have" pos="vvb" reg="have" xml:id="lbsw-001-a-2500">haue</w>
            <w lemma="so" pos="av" reg="so" xml:id="lbsw-001-a-2510">so</w>
            <w lemma="hard" pos="j" reg="hard" xml:id="lbsw-001-a-2520">hard</w>
            <w lemma="travel" pos="n1" reg="travel" xml:id="lbsw-001-a-2530">trauell</w>
            <pc xml:id="lbsw-001-a-2540">,</pc>
            <w lemma="that" pos="cs" reg="that" xml:id="lbsw-001-a-2550">that</w>
            <w lemma="the" pos="d" reg="the" xml:id="lbsw-001-a-2560">the</w>
            <w lemma="midwife" pos="n1" reg="Midwife" xml:id="lbsw-001-a-2570">Mid-wife</w>
            <w lemma="can" pos="vmb-x" reg="cannot" xml:id="lbsw-001-a-2580">cannot</w>
            <w lemma="perform" pos="vvi" reg="perform" xml:id="lbsw-001-a-2590">performe</w>
            <w lemma="her" pos="po" reg="her" xml:id="lbsw-001-a-2600">her</w>
            <w lemma="office" pos="n1" reg="office" xml:id="lbsw-001-a-2610">office</w>
            <pc xml:id="lbsw-001-a-2620">,</pc>
            <w lemma="those" pos="d" reg="those" xml:id="lbsw-001-a-2630">those</w>
            <w lemma="i" pos="pns" reg="I" xml:id="lbsw-001-a-2640">I</w>
            <w lemma="case" pos="n1" reg="case" xml:id="lbsw-001-a-2650">case</w>
            <pc xml:id="lbsw-001-a-2660">,</pc>
            <w lemma="and" pos="cc" reg="and" xml:id="lbsw-001-a-2670">and</w>
            <w lemma="deliver" pos="vvi" reg="deliver" xml:id="lbsw-001-a-2680">deliuer</w>
            <w lemma="present" pos="av_j" reg="presently" xml:id="lbsw-001-a-2690">presently</w>
            <pc unit="sentence" xml:id="lbsw-001-a-2700">.</pc>
          </item>
          <item xml:id="lbsw-e160">
            <w lemma="fistulaes" pos="n1g-nn" reg="Fistulaes'" xml:id="lbsw-001-a-2710">Fistulaes</w>
            <pc xml:id="lbsw-001-a-2720">,</pc>
            <w lemma="or" pos="cc" reg="or" xml:id="lbsw-001-a-2730">or</w>
            <w lemma="cankerous" pos="j" reg="cankerous" xml:id="lbsw-001-a-2740">cankerous</w>
            <w lemma="matter" pos="n2" reg="matters" xml:id="lbsw-001-a-2750">matters</w>
            <pc xml:id="lbsw-001-a-2760">,</pc>
            <w lemma="as" pos="acp-cs" reg="as" xml:id="lbsw-001-a-2770">as</w>
            <hi xml:id="lbsw-e170">
              <w lemma="Lupus" pos="n1-nn" reg="Lupus" xml:id="lbsw-001-a-2780">Lupus</w>
              <pc xml:id="lbsw-001-a-2790">,</pc>
              <w lemma="noli" pos="fw-la" reg="Noli" xml:id="lbsw-001-a-2800">Noli</w>
              <w lemma="i" pos="pno" reg="me" xml:id="lbsw-001-a-2810">me</w>
              <w lemma="tangere" pos="fw-la" reg="tangere" xml:id="lbsw-001-a-2820">tangere</w>
              <pc xml:id="lbsw-001-a-2830">,</pc>
            </hi>
            <w lemma="and" pos="cc" reg="and" xml:id="lbsw-001-a-2840">and</w>
            <w lemma="diverse" pos="j" reg="diverse" xml:id="lbsw-001-a-2850">diuers</w>
            <w lemma="external" pos="j" reg="external" xml:id="lbsw-001-a-2860">externall</w>
            <w lemma="and" pos="cc" reg="and" xml:id="lbsw-001-a-2870">and</w>
            <w lemma="enternall" pos="j" reg="enternall" xml:id="lbsw-001-a-2880">enternall</w>
            <w lemma="disease" pos="n2" reg="diseases" xml:id="lbsw-001-a-2890">diseases</w>
            <pc xml:id="lbsw-001-a-2900">,</pc>
            <w lemma="too" pos="av" reg="too" xml:id="lbsw-001-a-2910">too</w>
            <w lemma="long" pos="av_j" reg="long" xml:id="lbsw-001-a-2920">long</w>
            <w lemma="here" pos="av" reg="here" xml:id="lbsw-001-a-2930">héere</w>
            <w lemma="to" pos="acp-cs" reg="to" xml:id="lbsw-001-a-2940">to</w>
            <w lemma="rehearse" pos="vvi" reg="rehearse" xml:id="lbsw-001-a-2950">rehearse</w>
            <pc unit="sentence" xml:id="lbsw-001-a-2960">.</pc>
          </item>
          <item xml:id="lbsw-e180">
            <w lemma="the" pos="d" reg="The" xml:id="lbsw-001-a-2970">The</w>
            <w lemma="professor" pos="n1" reg="Professor" xml:id="lbsw-001-a-2980">Professor</w>
            <w lemma="hereof" pos="av" reg="hereof" xml:id="lbsw-001-a-2990">héereof</w>
            <w lemma="can" pos="vmb" reg="can" xml:id="lbsw-001-a-3000">can</w>
            <w lemma="show" pos="vvi" reg="show" xml:id="lbsw-001-a-3010">shewe</w>
            <w lemma="credible" pos="j" reg="credible" xml:id="lbsw-001-a-3020">credible</w>
            <w lemma="proof" pos="n2" reg="proofs" xml:id="lbsw-001-a-3030">proofes</w>
            <w lemma="for" pos="acp-p" reg="for" xml:id="lbsw-001-a-3040">for</w>
            <w lemma="the" pos="d" reg="the" xml:id="lbsw-001-a-3050">the</w>
            <w lemma="performance" pos="n1" reg="performance" xml:id="lbsw-001-a-3060">performance</w>
            <w lemma="of" pos="acp-p" reg="of" xml:id="lbsw-001-a-3070">of</w>
            <w lemma="these" pos="d" reg="these" xml:id="lbsw-001-a-3080">these</w>
            <w lemma="grief" pos="n2" reg="griefs" xml:id="lbsw-001-a-3090">greefes</w>
            <w lemma="before" pos="acp-av" reg="before" xml:id="lbsw-001-a-3100">before</w>
            <w lemma="mention" pos="vvn" reg="mentioned" xml:id="lbsw-001-a-3110">mentioned</w>
            <pc unit="sentence" xml:id="lbsw-001-a-3120">.</pc>
          </item>
        </list>
        <p xml:id="lbsw-e190">
          <hi xml:id="lbsw-e200">
            <w lemma="those" pos="d" reg="Those" xml:id="lbsw-001-a-3130">Those</w>
            <w lemma="which" pos="crq-r" reg="which" xml:id="lbsw-001-a-3140">which</w>
            <w lemma="shall" pos="vmb" reg="shall" xml:id="lbsw-001-a-3150">shall</w>
            <w lemma="have" pos="vvi" reg="have" xml:id="lbsw-001-a-3160">haue</w>
            <w lemma="need" pos="n1" reg="need" xml:id="lbsw-001-a-3170">neede</w>
            <w lemma="of" pos="acp-p" reg="of" xml:id="lbsw-001-a-3180">of</w>
            <w lemma="i" pos="pno" reg="me" xml:id="lbsw-001-a-3190">me</w>
            <pc xml:id="lbsw-001-a-3200">,</pc>
            <w lemma="shall" pos="vmb" reg="shall" xml:id="lbsw-001-a-3210">shall</w>
            <w lemma="have" pos="vvi" reg="have" xml:id="lbsw-001-a-3220">haue</w>
            <w lemma="i" pos="pno" reg="me" xml:id="lbsw-001-a-3230">me</w>
          </hi>
        </p>
      </div>
    </body>
    <back xml:id="lbsw-t-b"/>
  </text>
</TEI>

