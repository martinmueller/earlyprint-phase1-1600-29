<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>The portracture of Sir Thomas Overbury, Knight, ætat 32</title>
        <author>W. B.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1616</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A08612</idno>
        <idno type="STC">STC 18921.3</idno>
        <idno type="STC">ESTC S4105</idno>
        <idno type="EEBO-CITATION">34387237</idno>
        <idno type="OCLC">ocm 34387237</idno>
        <idno type="VID">29164</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A08612)</note>
        <note>Transcribed from: (Early English Books Online ; image set 29164)</note>
        <note>Images scanned from microfilm: (Early English books, 1475-1640 ; 1901:14)</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>The portracture of Sir Thomas Overbury, Knight, ætat 32</title>
            <author>W. B.</author>
            <author>Elstracke, Renold, fl. 1590-1630.</author>
            <author>Holland, Compton.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.) : port.</extent>
          <publicationStmt>
            <publisher>Compton Holland,</publisher>
            <pubPlace>London :</pubPlace>
            <date>[1616?]</date>
          </publicationStmt>
          <notesStmt>
            <note>Broadside portrait of Sir Thomas Overbury, showing him writing his own epitaph, with verse at bottom of sheet signed: W.B. The portrait signed at lower edge: Renold Elstrack sculpsit, Compton Holland excudit.</note>
            <note>Date of publication suggested by STC (2nd ed.).</note>
            <note>Imperfect: slightly faded.</note>
            <note>Reproduction of original in: Society of Antiquaries.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Overbury, Thomas, -- Sir, 1581-1613.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-08</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-10</date><label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2008-01</date><label>Judith Siefring</label>
        Sampled and proofread
      </change>
      <change><date>2008-01</date><label>Judith Siefring</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="mbzy-t">
    <body xml:id="mbzy-e0">
      <div type="epitaph" xml:id="mbzy-e10">
        <pb facs="tcp:29164:1" xml:id="mbzy-001-a"/>
        <head xml:id="mbzy-e20">
          <w lemma="the" pos="d" reg="THE" xml:id="mbzy-001-a-0010">THE</w>
          <w lemma="portraiture" pos="n1" reg="Portraiture" xml:id="mbzy-001-a-0020">Portracture</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mbzy-001-a-0030">of</w>
          <w lemma="sir" pos="n1" reg="Sir" xml:id="mbzy-001-a-0040">Sir</w>
          <hi xml:id="mbzy-e30">
            <w lemma="THOMAS" pos="n1-nn" reg="THOMAS" xml:id="mbzy-001-a-0050">THOMAS</w>
            <w lemma="overbury" pos="n1-nn" reg="OVERBURY" xml:id="mbzy-001-a-0060">OVERBVRY</w>
          </hi>
          <w lemma="knight" pos="n1" reg="Knight" xml:id="mbzy-001-a-0070">Knight</w>
          <pc unit="sentence" xml:id="mbzy-001-a-0080">.</pc>
          <w lemma="aetat" pos="fw-la" reg="AETAT" xml:id="mbzy-001-a-0090">AETAT</w>
          <pc unit="sentence" xml:id="mbzy-001-a-0100">.</pc>
          <w lemma="32." pos="crd" reg="32." xml:id="mbzy-001-a-0110">32.</w>
          <pc unit="sentence" xml:id="mbzy-001-a-0120"/>
        </head>
        <p xml:id="mbzy-e40">
          <figure xml:id="mbzy-e50">
            <figDesc xml:id="mbzy-e60">Overbury blazon or coat of arms</figDesc>
          </figure>
          <figure xml:id="mbzy-e70">
            <q xml:id="mbzy-e80">
              <floatingText xml:id="mbzy-e90">
                <body xml:id="mbzy-e100">
                  <div type="epitaph" xml:id="mbzy-e110">
                    <head xml:id="mbzy-e120">
                      <w lemma="his" pos="po" reg="His" xml:id="mbzy-001-a-0130">His</w>
                      <w lemma="epitaph" pos="n1" reg="Epitaph" xml:id="mbzy-001-a-0140">Epitaph</w>
                      <w lemma="write" pos="vvn" reg="written" xml:id="mbzy-001-a-0150">written</w>
                      <w lemma="by" pos="acp-p" reg="by" xml:id="mbzy-001-a-0160">by</w>
                      <w lemma="himself" pos="px" reg="himself" xml:id="mbzy-001-a-0170">himselfe</w>
                    </head>
                    <l xml:id="mbzy-e130">
                      <w lemma="the" pos="d" reg="The" xml:id="mbzy-001-a-0180">The</w>
                      <w lemma="span" pos="n1" reg="Span" xml:id="mbzy-001-a-0190">Span</w>
                      <w lemma="of" pos="acp-p" reg="of" xml:id="mbzy-001-a-0200">of</w>
                      <w lemma="my" pos="po" reg="my" xml:id="mbzy-001-a-0210">my</w>
                      <w lemma="day" pos="n2" reg="days" xml:id="mbzy-001-a-0220">dayes</w>
                      <w lemma="meassure" pos="vvn" reg="meassured" xml:id="mbzy-001-a-0230">meassur'd</w>
                      <pc xml:id="mbzy-001-a-0240">,</pc>
                      <w lemma="here" pos="av" reg="here" xml:id="mbzy-001-a-0250">heere</w>
                      <w lemma="i" pos="pns" reg="I" xml:id="mbzy-001-a-0260">I</w>
                      <w lemma="rest" pos="vvb" reg="rest" xml:id="mbzy-001-a-0270">rest</w>
                    </l>
                    <l xml:id="mbzy-e140">
                      <w lemma="that" pos="cs" reg="That" xml:id="mbzy-001-a-0280">That</w>
                      <w lemma="be" pos="vvz" reg="is" xml:id="mbzy-001-a-0290">is</w>
                      <pc xml:id="mbzy-001-a-0300">,</pc>
                      <w lemma="my" pos="po" reg="my" xml:id="mbzy-001-a-0310">my</w>
                      <w lemma="body" pos="n1" reg="body" xml:id="mbzy-001-a-0320">body</w>
                      <pc xml:id="mbzy-001-a-0330">:</pc>
                      <w lemma="but" pos="acp-cc" reg="but" xml:id="mbzy-001-a-0340">but</w>
                      <w lemma="my" pos="po" reg="my" xml:id="mbzy-001-a-0350">my</w>
                      <w lemma="soul" pos="n1" reg="soul" xml:id="mbzy-001-a-0360">soule</w>
                      <pc xml:id="mbzy-001-a-0370">,</pc>
                      <w lemma="his" pos="po" reg="his" xml:id="mbzy-001-a-0380">his</w>
                      <w lemma="●est" pos="n1" reg="●est" xml:id="mbzy-001-a-0390">●est</w>
                      <pc xml:id="mbzy-001-a-0400">,</pc>
                    </l>
                    <l xml:id="mbzy-e150">
                      <w lemma="be" pos="vvz" reg="Is" xml:id="mbzy-001-a-0410">Is</w>
                      <w lemma="hence" pos="av" reg="hence" xml:id="mbzy-001-a-0420">hence</w>
                      <w lemma="ascend" pos="vvn" reg="ascended" xml:id="mbzy-001-a-0430">ascended</w>
                      <w lemma="where" pos="crq-cs" reg="where" xml:id="mbzy-001-a-0440">where</w>
                      <w lemma="neither" pos="d-x" reg="neither" xml:id="mbzy-001-a-0450">neither</w>
                      <w lemma="time" pos="n1" reg="Time" xml:id="mbzy-001-a-0460">Tyme</w>
                      <pc xml:id="mbzy-001-a-0470">,</pc>
                    </l>
                    <l xml:id="mbzy-e160">
                      <w lemma="nor" pos="cc-x" reg="Nor" xml:id="mbzy-001-a-0480">Nor</w>
                      <w lemma="faith" pos="n1" reg="Faith" xml:id="mbzy-001-a-0490">Faith</w>
                      <pc xml:id="mbzy-001-a-0500">:</pc>
                      <w lemma="nor" pos="cc-x" reg="nor" xml:id="mbzy-001-a-0510">nor</w>
                      <w lemma="hope" pos="vvb" reg="Hope" xml:id="mbzy-001-a-0520">Hope</w>
                      <pc xml:id="mbzy-001-a-0530">,</pc>
                      <w lemma="but" pos="acp-cc" reg="but" xml:id="mbzy-001-a-0540">but</w>
                      <w lemma="only" pos="j" reg="only" xml:id="mbzy-001-a-0550">onely</w>
                      <w lemma="love" pos="n1" reg="love" xml:id="mbzy-001-a-0560">loue</w>
                      <w lemma="can" pos="vmb" reg="can" xml:id="mbzy-001-a-0570">can</w>
                      <w lemma="climb" pos="vvi" reg="climb" xml:id="mbzy-001-a-0580">clime</w>
                      <pc xml:id="mbzy-001-a-0590">,</pc>
                    </l>
                    <l xml:id="mbzy-e170">
                      <w lemma="where" pos="crq-cs" reg="Where" xml:id="mbzy-001-a-0600">Where</w>
                      <w lemma="being" pos="n1" reg="being" xml:id="mbzy-001-a-0610">being</w>
                      <w lemma="more" pos="av-c_d" reg="more" xml:id="mbzy-001-a-0620">more</w>
                      <w lemma="enlighten" pos="vvn" reg="enlightened" xml:id="mbzy-001-a-0630">enlightned</w>
                      <pc xml:id="mbzy-001-a-0640">,</pc>
                      <w lemma="shee" pos="pns" reg="She" xml:id="mbzy-001-a-0650">Shee</w>
                      <w lemma="do" pos="vvz" reg="doth" xml:id="mbzy-001-a-0660">doth</w>
                      <w lemma="know" pos="vvi" reg="know" xml:id="mbzy-001-a-0670">know</w>
                    </l>
                    <l xml:id="mbzy-e180">
                      <w lemma="the" pos="d" reg="The" xml:id="mbzy-001-a-0680">The</w>
                      <w lemma="truth" pos="n1" reg="truth" xml:id="mbzy-001-a-0690">truth</w>
                      <w lemma="of" pos="acp-p" reg="of" xml:id="mbzy-001-a-0700">of</w>
                      <w lemma="all" pos="d" reg="all" xml:id="mbzy-001-a-0710">all</w>
                      <pc xml:id="mbzy-001-a-0720">,</pc>
                      <w lemma="man" pos="n2" reg="men" xml:id="mbzy-001-a-0730">men</w>
                      <w lemma="argue" pos="vvb" reg="argue" xml:id="mbzy-001-a-0740">argue</w>
                      <w lemma="of" pos="acp-p" reg="of" xml:id="mbzy-001-a-0750">of</w>
                      <w lemma="below" pos="acp-av" reg="below" xml:id="mbzy-001-a-0760">below</w>
                      <pc xml:id="mbzy-001-a-0770">:</pc>
                    </l>
                    <l xml:id="mbzy-e190">
                      <w lemma="only" pos="av_j" reg="Only" xml:id="mbzy-001-a-0780">Onely</w>
                      <w lemma="this" pos="d" reg="this" xml:id="mbzy-001-a-0790">this</w>
                      <w lemma="dust" pos="n1" reg="dust" xml:id="mbzy-001-a-0800">dust</w>
                      <w lemma="do" pos="vvz" reg="doth" xml:id="mbzy-001-a-0810">doth</w>
                      <w lemma="here" pos="av" reg="here" xml:id="mbzy-001-a-0820">heere</w>
                      <w lemma="in" pos="acp-p" reg="in" xml:id="mbzy-001-a-0830">in</w>
                      <w lemma="〈◊〉" pos="n1" reg="〈◊〉" xml:id="mbzy-001-a-0840">〈◊〉</w>
                      <w lemma="remain" pos="vvi" reg="remain" xml:id="mbzy-001-a-0850">remaine</w>
                      <pc xml:id="mbzy-001-a-0860">,</pc>
                    </l>
                    <l xml:id="mbzy-e200">
                      <w lemma="that" pos="cs" reg="That" xml:id="mbzy-001-a-0870">That</w>
                      <w lemma="when" pos="crq-cs" reg="when" xml:id="mbzy-001-a-0880">when</w>
                      <w lemma="the" pos="d" reg="the" xml:id="mbzy-001-a-0890">the</w>
                      <w lemma="world" pos="n1" reg="world" xml:id="mbzy-001-a-0900">world</w>
                      <w lemma="dissolve" pos="vvz" reg="dissolves" xml:id="mbzy-001-a-0910">dissolues</w>
                      <w lemma="she" pos="pns" reg="she" xml:id="mbzy-001-a-0920">she</w>
                      <w lemma="come" pos="vvn" reg="come" xml:id="mbzy-001-a-0930">come</w>
                      <w lemma="again" pos="av" reg="again" xml:id="mbzy-001-a-0940">againe</w>
                      <pc unit="sentence" xml:id="mbzy-001-a-0950">.</pc>
                    </l>
                    <closer xml:id="mbzy-e210">
                      <signed xml:id="mbzy-e220">
                        <w lemma="Tho" pos="n1-nn" reg="Tho" xml:id="mbzy-001-a-0960">Tho</w>
                        <pc xml:id="mbzy-001-a-0970">:</pc>
                        <w lemma="Ouerburie" pos="n1-nn" reg="Ouerburie" xml:id="mbzy-001-a-0980">Ouerburie</w>
                      </signed>
                    </closer>
                  </div>
                </body>
              </floatingText>
            </q>
            <p xml:id="mbzy-e230">
              <w lemma="Compton" pos="n1-nn" reg="Compton" xml:id="mbzy-001-a-0990">Compton</w>
              <w lemma="Holland" pos="n1-nn" reg="Holland" xml:id="mbzy-001-a-1000">Holland</w>
              <w lemma="excudit" pos="fw-la" reg="excudit" xml:id="mbzy-001-a-1010">excudit</w>
            </p>
          </figure>
        </p>
        <lg xml:id="mbzy-e240">
          <l xml:id="mbzy-e250">
            <w lemma="those" pos="d" reg="Those" xml:id="mbzy-001-a-1020">Those</w>
            <w lemma="swan-like" pos="av_j" reg="Swanlike" xml:id="mbzy-001-a-1030">Swan-like</w>
            <w lemma="note" pos="n2" reg="notes" xml:id="mbzy-001-a-1040">notes</w>
            <pc xml:id="mbzy-001-a-1050">,</pc>
            <w lemma="sing" pos="vvn" reg="sung" xml:id="mbzy-001-a-1060">sung</w>
            <w lemma="so" pos="av" reg="so" xml:id="mbzy-001-a-1070">so</w>
            <w lemma="inspire" pos="av_vn" reg="inspiredly" xml:id="mbzy-001-a-1080">inspiredly</w>
          </l>
          <l xml:id="mbzy-e260">
            <w lemma="to" pos="acp-p" reg="to" xml:id="mbzy-001-a-1090">to</w>
            <w lemma="thy" pos="po" reg="thy" xml:id="mbzy-001-a-1100">thy</w>
            <w lemma="untimely" pos="j" reg="untimely" xml:id="mbzy-001-a-1110">vntimely</w>
            <w lemma="fall" pos="n1" reg="fall" xml:id="mbzy-001-a-1120">fall</w>
            <pc xml:id="mbzy-001-a-1130">,</pc>
            <w lemma="prove" pos="vvb" reg="prove" xml:id="mbzy-001-a-1140">proue</w>
            <w lemma="most" pos="av-s_d" reg="most" xml:id="mbzy-001-a-1150">most</w>
            <w lemma="exact" pos="j" reg="exact" xml:id="mbzy-001-a-1160">exact</w>
          </l>
          <l xml:id="mbzy-e270">
            <w lemma="line" pos="n1" reg="line" xml:id="mbzy-001-a-1170" type="contract1">Line</w><w xml:id="mbzy-001-a-1170c" type="contract2" lemma="be" pos="vvz" reg="'s" join="left">'s</w>
            <w lemma="draw" pos="vvn" reg="drawn" xml:id="mbzy-001-a-1180">drawne</w>
            <w lemma="from" pos="acp-p" reg="from" xml:id="mbzy-001-a-1190">from</w>
            <w lemma="life" pos="n1" reg="Life" xml:id="mbzy-001-a-1200" rendition="#hi">Life</w>
            
            <pc xml:id="mbzy-001-a-1210">:</pc>
            <w xml:id="mbzy-001-a-1220" lemma="&amp;" reg="&amp;" pos="cc">&amp;</w>
            <w lemma="thy" pos="po" reg="thy" xml:id="mbzy-001-a-1230">thy</w>
            <w lemma="swift" pos="j" reg="swift" xml:id="mbzy-001-a-1240">swift</w>
            <w lemma="tragedy" pos="n1" reg="Tragedy" xml:id="mbzy-001-a-1250" rendition="#hi">Tragedie</w>
            
          </l>
          <l xml:id="mbzy-e300">
            <w lemma="show" pos="vvz" reg="shows" xml:id="mbzy-001-a-1260">showes</w>
            <w lemma="but" pos="acp-av" reg="but" xml:id="mbzy-001-a-1270">but</w>
            <w lemma="thy" pos="po" reg="thy" xml:id="mbzy-001-a-1280">thine</w>
            <w lemma="own" pos="d" reg="own" xml:id="mbzy-001-a-1290">owne</w>
            <w lemma="soul" pos="n2" reg="Souls" xml:id="mbzy-001-a-1300">Soules</w>
            <w lemma="prophesy" pos="vvb" reg="Prophesy" xml:id="mbzy-001-a-1310" rendition="#hi">Prophecie</w>
            
            <w lemma="in" pos="acp-p" reg="in" xml:id="mbzy-001-a-1320">in</w>
            <w lemma="act." pos="n-ab" reg="Act." xml:id="mbzy-001-a-1330" rendition="#hi">Act.</w>
            
          </l>
          <l xml:id="mbzy-e330">
            <w lemma="thy" pos="po" reg="Thy" xml:id="mbzy-001-a-1340">Thy</w>
            <w lemma="name" pos="n1" reg="Name" xml:id="mbzy-001-a-1350" rendition="#hi">Name</w><pc xml:id="mbzy-001-a-1360">,</pc>
            
              
            <w lemma="and" pos="cc" reg="and" xml:id="mbzy-001-a-1370">and</w>
            <w lemma="virtue" pos="n2" reg="Virtues" xml:id="mbzy-001-a-1380" rendition="#hi">Vertues</w>
            
            <w lemma="five" pos="crd" reg="five" xml:id="mbzy-001-a-1390">fiue</w>
            <pc xml:id="mbzy-001-a-1400">:</pc>
            <w lemma="to" pos="acp-cs" reg="To" xml:id="mbzy-001-a-1410">To</w>
            <w lemma="kill" pos="vvi" reg="kill" xml:id="mbzy-001-a-1420">kill</w>
            <w lemma="thy" pos="po" reg="thy" xml:id="mbzy-001-a-1430">thy</w>
            <w lemma="mould" pos="n1" reg="Mould" xml:id="mbzy-001-a-1440">Mould</w>
          </l>
          <l xml:id="mbzy-e360">
            <w lemma="be" pos="vvd" reg="was" xml:id="mbzy-001-a-1450">was</w>
            <w lemma="all" pos="d" reg="all" xml:id="mbzy-001-a-1460">all</w>
            <w lemma="imprisonment" pos="n1" reg="Imprisonment" xml:id="mbzy-001-a-1470">Imprisonment</w>
            <pc xml:id="mbzy-001-a-1480">,</pc>
            <w lemma="and" pos="cc" reg="and" xml:id="mbzy-001-a-1490">and</w>
            <w lemma="poison" pos="n1" reg="Poison" xml:id="mbzy-001-a-1500">Poyson</w>
            <w lemma="can" pos="vmd" reg="could" xml:id="mbzy-001-a-1510">could</w>
            <pc unit="sentence" xml:id="mbzy-001-a-1520">.</pc>
          </l>
        </lg>
        <lg xml:id="mbzy-e370">
          <l xml:id="mbzy-e380">
            <w lemma="but" pos="acp-p" reg="But" xml:id="mbzy-001-a-1530">But</w>
            <w lemma="thy" pos="po" reg="thy" xml:id="mbzy-001-a-1540">thy</w>
            <w lemma="more-heaven" pos="av" reg="more-heavenly" xml:id="mbzy-001-a-1550">more-heauenly</w>
            <pc xml:id="mbzy-001-a-1560">-</pc>
            <w lemma="self" pos="n1" reg="Self" xml:id="mbzy-001-a-1570" rendition="#hi">Self</w><pc xml:id="mbzy-001-a-1580">,</pc>
            
              
            <w lemma="from" pos="acp-p" reg="from" xml:id="mbzy-001-a-1590">from</w>
            <w lemma="double" pos="j" reg="double" xml:id="mbzy-001-a-1600">double</w>
            <w lemma="chain" pos="n2" reg="chains" xml:id="mbzy-001-a-1610">chaines</w>
          </l>
          <l xml:id="mbzy-e400">
            <w lemma="set" pos="vvd" reg="set" xml:id="mbzy-001-a-1620">sett</w>
            <w lemma="free" pos="j" reg="free" xml:id="mbzy-001-a-1630">free</w>
            <pc xml:id="mbzy-001-a-1640" join="right">(</pc>
            <w lemma="at" pos="acp-p" reg="at" xml:id="mbzy-001-a-1650">at</w>
            <w lemma="once" pos="acp-av" reg="once" xml:id="mbzy-001-a-1660">once</w>
            <pc xml:id="mbzy-001-a-1670">)</pc>
            <w lemma="thy" pos="po" reg="Thy" xml:id="mbzy-001-a-1680">Thy</w>
            <w lemma="body" pos="n1" reg="Body" xml:id="mbzy-001-a-1690" rendition="#hi">Body</w><pc xml:id="mbzy-001-a-1700">,</pc>
            
              
            <w lemma="and" pos="cc" reg="and" xml:id="mbzy-001-a-1710">and</w>
            <w lemma="the" pos="d" reg="the" xml:id="mbzy-001-a-1720">the</w>
            <w lemma="tower" pos="n1" reg="Tower" xml:id="mbzy-001-a-1730" rendition="#hi">Tower</w><pc xml:id="mbzy-001-a-1740">,</pc>
            
              
          </l>
          <l xml:id="mbzy-e430">
            <w lemma="in" pos="acp-p" reg="In" xml:id="mbzy-001-a-1750">In</w>
            <w lemma="that" pos="d" reg="that" xml:id="mbzy-001-a-1760">that</w>
            <w lemma="supreme" pos="j" reg="Supreme" xml:id="mbzy-001-a-1770">Supreme</w>
            <w lemma="unpartial" pos="j" reg="unpartial" xml:id="mbzy-001-a-1780">vnpartiall</w>
            <w lemma="court" pos="n1" reg="Court" xml:id="mbzy-001-a-1790" rendition="#hi">Court</w>
            
            <w lemma="remain" pos="vvz" reg="remains" xml:id="mbzy-001-a-1800">remaines</w>
            <pc xml:id="mbzy-001-a-1810">,</pc>
          </l>
          <l xml:id="mbzy-e450">
            <w lemma="where" pos="crq-cs" reg="where" xml:id="mbzy-001-a-1820">wher</w>
            <w lemma="nor" pos="cc-x" reg="nor" xml:id="mbzy-001-a-1830">nor</w>
            <hi xml:id="mbzy-e460">
              <w lemma="ambition" pos="n1" reg="Ambition" xml:id="mbzy-001-a-1840">Ambition</w>
              <pc xml:id="mbzy-001-a-1850">,</pc>
              <w lemma="envy" pos="n1" reg="Envy" xml:id="mbzy-001-a-1860">Enuy</w>
              <w lemma="lust" pos="n1" reg="Lust" xml:id="mbzy-001-a-1870">Lust</w>
            </hi>
            <w lemma="have" pos="vvb" reg="have" xml:id="mbzy-001-a-1880">haue</w>
            <w lemma="power" pos="n1" reg="power" xml:id="mbzy-001-a-1890">power</w>
            <pc xml:id="mbzy-001-a-1900">:</pc>
          </l>
          <l xml:id="mbzy-e470">
            <w lemma="redeem" pos="j_vn" reg="Redeemed" xml:id="mbzy-001-a-1910">Redeem'd</w>
            <w lemma="from" pos="acp-p" reg="from" xml:id="mbzy-001-a-1920">from</w>
            <w lemma="poisonous" pos="j" reg="poisonous" xml:id="mbzy-001-a-1930">poysonous</w>
            <w lemma="plot" pos="n2" reg="plots" xml:id="mbzy-001-a-1940">plotts</w>
            <pc xml:id="mbzy-001-a-1950">,</pc>
            <w lemma="from" pos="acp-p" reg="from" xml:id="mbzy-001-a-1960">from</w>
            <w lemma="witch" pos="n2" reg="Witches" xml:id="mbzy-001-a-1970" rendition="#hi">Witches</w>
            
            <w lemma="charm" pos="n2" reg="charms" xml:id="mbzy-001-a-1980">charmes</w>
            <pc xml:id="mbzy-001-a-1990">,</pc>
          </l>
          <l xml:id="mbzy-e490">
            <w lemma="from" pos="acp-p" reg="from" xml:id="mbzy-001-a-2000">from</w>
            <w lemma="weston" pos="n1g-nn" reg="Weston's" xml:id="mbzy-001-a-2010" rendition="#hi">Weston's</w>
            
            <w xml:id="mbzy-001-a-2020" lemma="&amp;" reg="&amp;" pos="cc">&amp;</w>
            <w lemma="the" pos="d" reg="the" xml:id="mbzy-001-a-2030">th'</w>
            <w lemma="apothecary" pos="n1g" reg="Apothecary's" xml:id="mbzy-001-a-2040" rendition="#hi">Apothecaries</w>
            
            <w lemma="harm" pos="n2" reg="harms" xml:id="mbzy-001-a-2050">harmes</w>
            <pc unit="sentence" xml:id="mbzy-001-a-2060">.</pc>
          </l>
        </lg>
        <closer xml:id="mbzy-e520">
          <signed xml:id="mbzy-e530">
            <w lemma="w." pos="n-ab" reg="W." xml:id="mbzy-001-a-2070">W.</w>
            <w lemma="p." pos="n-ab" reg="P." xml:id="mbzy-001-a-2080">P.</w>
            <pc unit="sentence" xml:id="mbzy-001-a-2090"/>
          </signed>
        </closer>
      </div>
    </body>
    <back xml:id="mbzy-t-b"/>
  </text>
</TEI>

