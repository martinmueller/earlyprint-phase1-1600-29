<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>To all mayors, iurates, constables, bayliffes, ministers, churchwardens, and to all other his maiesties officers within the libertie of the Cinque Ports, as it shall appertain</title>
        <author>Zouche of Harringworth, Edward La Zouche, Baron, 1556?-1625.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1619</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A18899</idno>
        <idno type="STC">STC 5323A.2</idno>
        <idno type="STC">ESTC S3357</idno>
        <idno type="EEBO-CITATION">33143362</idno>
        <idno type="OCLC">ocm 33143362</idno>
        <idno type="VID">28416</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A18899)</note>
        <note>Transcribed from: (Early English Books Online ; image set 28416)</note>
        <note>Images scanned from microfilm: (Early English books, 1475-1640 ; 1885:14)</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>To all mayors, iurates, constables, bayliffes, ministers, churchwardens, and to all other his maiesties officers within the libertie of the Cinque Ports, as it shall appertain</title>
            <author>Zouche of Harringworth, Edward La Zouche, Baron, 1556?-1625.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.).</extent>
          <publicationStmt>
            <publisher>T. Purfoot],</publisher>
            <pubPlace>[London :</pubPlace>
            <date>1619.</date>
          </publicationStmt>
          <notesStmt>
            <note>Patent for a collection to repair harbors in Dunwich, Southwold, and Walberswick.</note>
            <note>Place and publisher from STC (2nd ed.).</note>
            <note>Reproduction of original in: Society of Antiquaries.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Harbors -- Maintenance and repair -- Finance -- England.</term>
          <term>Cinque Ports (England) -- History.</term>
          <term>Great Britain -- History -- James I, 1603-1625.</term>
          <term>Broadsides -- London (England) -- 16th century.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-08</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-08</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-09</date><label>Elspeth Healey</label>
        Sampled and proofread
      </change>
      <change><date>2007-09</date><label>Elspeth Healey</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="mdfe-t">
    <body xml:id="mdfe-e0">
      <div type="letter" xml:id="mdfe-e10">
        <pb facs="tcp:28416:1" xml:id="mdfe-001-a"/>
        <p xml:id="mdfe-e20">
          <w lemma="after" pos="acp-p" reg="After" rendition="#decorinit" xml:id="mdfe-001-a-0010">AFter</w>
          <w lemma="my" pos="po" reg="my" xml:id="mdfe-001-a-0020">my</w>
          <w lemma="very" pos="j" reg="very" xml:id="mdfe-001-a-0030">verie</w>
          <w lemma="hearty" pos="j" reg="hearty" xml:id="mdfe-001-a-0040">heartie</w>
          <w lemma="commendation" pos="n2" reg="Commendations" xml:id="mdfe-001-a-0050">Commendations</w>
          <pc xml:id="mdfe-001-a-0060">,</pc>
          <w lemma="since" pos="acp-cs" reg="Since" xml:id="mdfe-001-a-0070">Since</w>
          <w lemma="it" pos="pn" reg="it" xml:id="mdfe-001-a-0080">it</w>
          <w lemma="have" pos="vvz" reg="hath" xml:id="mdfe-001-a-0090">hath</w>
          <w lemma="please" pos="vvn" reg="pleased" xml:id="mdfe-001-a-0100">pleased</w>
          <w lemma="his" pos="po" reg="his" xml:id="mdfe-001-a-0110">his</w>
          <w lemma="majesty" pos="n1" reg="Majesty" xml:id="mdfe-001-a-0120">Maiesty</w>
          <w lemma="out" pos="av" reg="out" xml:id="mdfe-001-a-0130">out</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-0140">of</w>
          <w lemma="his" pos="po" reg="his" xml:id="mdfe-001-a-0150">his</w>
          <w lemma="gracious" pos="j" reg="gracious" xml:id="mdfe-001-a-0160">gracious</w>
          <w lemma="and" pos="cc" reg="and" xml:id="mdfe-001-a-0170">and</w>
          <w lemma="royal" pos="j" reg="Royal" xml:id="mdfe-001-a-0180">Royall</w>
          <w lemma="care" pos="n1" reg="care" xml:id="mdfe-001-a-0190">care</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-0200">of</w>
          <w lemma="the" pos="d" reg="the" xml:id="mdfe-001-a-0210">the</w>
          <w lemma="good" pos="j" reg="good" xml:id="mdfe-001-a-0220">good</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-0230">of</w>
          <w lemma="his" pos="po" reg="his" xml:id="mdfe-001-a-0240">his</w>
          <w lemma="kingdom" pos="n1" reg="Kingdom" xml:id="mdfe-001-a-0250">Kingdome</w>
          <w lemma="and" pos="cc" reg="and" xml:id="mdfe-001-a-0260">and</w>
          <w lemma="subject" pos="n2" reg="Subjects" xml:id="mdfe-001-a-0270">Subiects</w>
          <pc xml:id="mdfe-001-a-0280">,</pc>
          <w lemma="by" pos="acp-p" reg="by" xml:id="mdfe-001-a-0290">by</w>
          <w lemma="his" pos="po" reg="his" xml:id="mdfe-001-a-0300">his</w>
          <w lemma="letter" pos="n2" reg="Letters" xml:id="mdfe-001-a-0310">Letters</w>
          <w lemma="patent" pos="n2" reg="Patents" xml:id="mdfe-001-a-0320">Patents</w>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="mdfe-001-a-0330">to</w>
          <w lemma="recommend" pos="vvi" reg="recommend" xml:id="mdfe-001-a-0340">recommend</w>
          <w lemma="by" pos="acp-p" reg="by" xml:id="mdfe-001-a-0350">by</w>
          <w lemma="way" pos="n1" reg="way" xml:id="mdfe-001-a-0360">way</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-0370">of</w>
          <w lemma="collection" pos="n1" reg="Collection" xml:id="mdfe-001-a-0380">Collection</w>
          <pc xml:id="mdfe-001-a-0390">,</pc>
          <w lemma="the●epaire" pos="vvb" reg="the●epaire" xml:id="mdfe-001-a-0400">the●epaire</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-0410">of</w>
          <w lemma="the" pos="d" reg="the" xml:id="mdfe-001-a-0420">the</w>
          <w lemma="ancient" pos="j" reg="ancient" xml:id="mdfe-001-a-0430">ancient</w>
          <w lemma="haven" pos="n1" reg="Haven" xml:id="mdfe-001-a-0440">Hauen</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-0450">of</w>
          <w lemma="the" pos="d" reg="the" xml:id="mdfe-001-a-0460">the</w>
          <w lemma="coast" pos="n1" reg="Coast" xml:id="mdfe-001-a-0470">Coast</w>
          <w lemma="town" pos="n2" reg="towns" xml:id="mdfe-001-a-0480">townes</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-0490">of</w>
          <hi xml:id="mdfe-e30">
            <w lemma="dunwich" pos="n1-nn" reg="Dunwich" xml:id="mdfe-001-a-0500">Dunwich</w>
            <pc xml:id="mdfe-001-a-0510">,</pc>
            <w lemma="southwold" pos="n1-nn" reg="Southwold" xml:id="mdfe-001-a-0520">Southwold</w>
            <pc xml:id="mdfe-001-a-0530">,</pc>
          </hi>
          <w lemma="and" pos="cc" reg="and" xml:id="mdfe-001-a-0540">and</w>
          <hi xml:id="mdfe-e40">
            <w lemma="walberswick" pos="n1-nn" reg="Walberswick" xml:id="mdfe-001-a-0550">Walberswick</w>
          </hi>
          <w lemma="in" pos="acp-p" reg="in" xml:id="mdfe-001-a-0560">in</w>
          <w lemma="the" pos="d" reg="the" xml:id="mdfe-001-a-0570">the</w>
          <w lemma="county" pos="n1" reg="County" xml:id="mdfe-001-a-0580">Countie</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-0590">of</w>
          <hi xml:id="mdfe-e50">
            <w lemma="Suffolk" pos="n1-nn" reg="Suffolk" xml:id="mdfe-001-a-0600">Suffolke</w>
            <pc xml:id="mdfe-001-a-0610">,</pc>
          </hi>
          <w lemma="to" pos="acp-p" reg="to" xml:id="mdfe-001-a-0620">to</w>
          <w lemma="the" pos="d" reg="the" xml:id="mdfe-001-a-0630">the</w>
          <w lemma="charitable" pos="j" reg="charitable" xml:id="mdfe-001-a-0640">charitable</w>
          <w lemma="benevolence" pos="n1" reg="benevolence" xml:id="mdfe-001-a-0650">beneuolence</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-0660">of</w>
          <w lemma="all" pos="d" reg="all" xml:id="mdfe-001-a-0670">all</w>
          <w lemma="his" pos="po" reg="his" xml:id="mdfe-001-a-0680">his</w>
          <w lemma="love" pos="j_vg" reg="loving" xml:id="mdfe-001-a-0690">louing</w>
          <w lemma="subject" pos="n2" reg="Subjects" xml:id="mdfe-001-a-0700">Subiects</w>
          <w lemma="within" pos="acp-p" reg="within" xml:id="mdfe-001-a-0710">within</w>
          <w lemma="this" pos="d" reg="this" xml:id="mdfe-001-a-0720">this</w>
          <w lemma="his" pos="po" reg="his" xml:id="mdfe-001-a-0730">his</w>
          <w lemma="majesty" pos="n1g" reg="Majesty's" xml:id="mdfe-001-a-0740">Maiesties</w>
          <w lemma="realm" pos="n1" reg="Realm" xml:id="mdfe-001-a-0750">Realme</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-0760">of</w>
          <hi xml:id="mdfe-e60">
            <w lemma="England" pos="n1-nn" reg="England" xml:id="mdfe-001-a-0770">England</w>
            <pc xml:id="mdfe-001-a-0780">,</pc>
          </hi>
          <w lemma="and" pos="cc" reg="and" xml:id="mdfe-001-a-0790">and</w>
          <w lemma="principality" pos="n1" reg="Principality" xml:id="mdfe-001-a-0800">Principalitie</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-0810">of</w>
          <hi xml:id="mdfe-e70">
            <w lemma="Wales" pos="n1-nn" reg="Wales" xml:id="mdfe-001-a-0820">Wales</w>
            <pc xml:id="mdfe-001-a-0830">:</pc>
          </hi>
          <w lemma="i" pos="pns" reg="I" xml:id="mdfe-001-a-0840">I</w>
          <w lemma="think" pos="vvd" reg="thought" xml:id="mdfe-001-a-0850">thought</w>
          <w lemma="it" pos="pn" reg="it" xml:id="mdfe-001-a-0860">it</w>
          <w lemma="meet" pos="vvb" reg="meet" xml:id="mdfe-001-a-0870">meet</w>
          <pc xml:id="mdfe-001-a-0880" join="right">(</pc>
          <w lemma="be" pos="vvg" reg="being" xml:id="mdfe-001-a-0890">being</w>
          <w lemma="petition" pos="vvn" reg="petitioned" xml:id="mdfe-001-a-0900">petitioned</w>
          <w lemma="by" pos="acp-p" reg="by" xml:id="mdfe-001-a-0910">by</w>
          <w lemma="the" pos="d" reg="the" xml:id="mdfe-001-a-0920">the</w>
          <w lemma="partie●" pos="n1" reg="partie●" xml:id="mdfe-001-a-0930">partie●</w>
          <w lemma="name" pos="n1" reg="name" xml:id="mdfe-001-a-0940">name</w>
          <w lemma="in" pos="acp-p" reg="in" xml:id="mdfe-001-a-0950">in</w>
          <w lemma="the" pos="d" reg="the" xml:id="mdfe-001-a-0960">the</w>
          <w lemma="say" pos="j_vn" reg="said" xml:id="mdfe-001-a-0970">said</w>
          <w lemma="letter" pos="n2" reg="Letters" xml:id="mdfe-001-a-0980">Letters</w>
          <w lemma="patent" pos="n2" reg="Patents" xml:id="mdfe-001-a-0990">Patents</w>
          <pc xml:id="mdfe-001-a-1000">)</pc>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="mdfe-001-a-1010">to</w>
          <w lemma="give" pos="vvi" reg="give" xml:id="mdfe-001-a-1020">giue</w>
          <w lemma="this" pos="d" reg="this" xml:id="mdfe-001-a-1030">this</w>
          <w lemma="addition" pos="n1" reg="addition" xml:id="mdfe-001-a-1040">addition</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-1050">of</w>
          <w lemma="assistance" pos="n1" reg="assistance" xml:id="mdfe-001-a-1060">assistance</w>
          <w lemma="thereunto" pos="av" reg="thereunto" xml:id="mdfe-001-a-1070">thereunto</w>
          <pc xml:id="mdfe-001-a-1080">,</pc>
          <w lemma="though" pos="cs" reg="though" xml:id="mdfe-001-a-1090">though</w>
          <w lemma="there" pos="av" reg="there" xml:id="mdfe-001-a-1100">there</w>
          <w lemma="be" pos="vvi" reg="be" xml:id="mdfe-001-a-1110">bee</w>
          <w lemma="express" pos="vvn" reg="expressed" xml:id="mdfe-001-a-1120">expressed</w>
          <w lemma="in" pos="acp-p" reg="in" xml:id="mdfe-001-a-1130">in</w>
          <w lemma="the" pos="d" reg="the" xml:id="mdfe-001-a-1140">the</w>
          <w lemma="brief" pos="n2" reg="Briefs" xml:id="mdfe-001-a-1150">Briefes</w>
          <pc xml:id="mdfe-001-a-1160" join="right">(</pc>
          <w lemma="which" pos="crq-r" reg="which" xml:id="mdfe-001-a-1170">which</w>
          <w lemma="he" pos="pns" reg="he" xml:id="mdfe-001-a-1180">he</w>
          <w lemma="will" pos="vmb" reg="will" xml:id="mdfe-001-a-1190">will</w>
          <w lemma="deliver" pos="vvi" reg="deliver" xml:id="mdfe-001-a-1200">deliuer</w>
          <w lemma="you" pos="pn" reg="you" xml:id="mdfe-001-a-1210">you</w>
          <pc xml:id="mdfe-001-a-1220">)</pc>
          <w lemma="sufficient" pos="j" reg="sufficient" xml:id="mdfe-001-a-1230">sufficient</w>
          <w lemma="motive" pos="n2" reg="Motives" xml:id="mdfe-001-a-1240">Motiues</w>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="mdfe-001-a-1250">to</w>
          <w lemma="induce" pos="vvi" reg="induce" xml:id="mdfe-001-a-1260">induce</w>
          <w lemma="every" pos="d" reg="every" xml:id="mdfe-001-a-1270">euery</w>
          <w lemma="man" pos="n1" reg="man" xml:id="mdfe-001-a-1280">man</w>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="mdfe-001-a-1290">to</w>
          <w lemma="bestow" pos="vvi" reg="bestow" xml:id="mdfe-001-a-1300">bestowe</w>
          <w lemma="what" pos="crq-r" reg="what" xml:id="mdfe-001-a-1310">what</w>
          <w lemma="be" pos="vvz" reg="is" xml:id="mdfe-001-a-1320">is</w>
          <w lemma="fit" pos="vvg" reg="fitting" xml:id="mdfe-001-a-1330">fitting</w>
          <pc xml:id="mdfe-001-a-1340">,</pc>
          <w lemma="for" pos="acp-p" reg="for" xml:id="mdfe-001-a-1350">for</w>
          <w lemma="the" pos="d" reg="the" xml:id="mdfe-001-a-1360">the</w>
          <w lemma="advancement" pos="n1" reg="advancement" xml:id="mdfe-001-a-1370">aduancement</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-1380">of</w>
          <w lemma="so" pos="av" reg="so" xml:id="mdfe-001-a-1390">so</w>
          <w lemma="worthy" pos="j" reg="worthy" xml:id="mdfe-001-a-1400">worthie</w>
          <w lemma="a" pos="d" reg="an" xml:id="mdfe-001-a-1410">an</w>
          <w lemma="action" pos="n1" reg="action" xml:id="mdfe-001-a-1420">action</w>
          <pc unit="sentence" xml:id="mdfe-001-a-1430">.</pc>
          <w lemma="wish" pos="vvg" reg="Wishing" xml:id="mdfe-001-a-1440">Wishing</w>
          <w lemma="and" pos="cc" reg="and" xml:id="mdfe-001-a-1450">and</w>
          <w lemma="desire" pos="vvg" reg="desiring" xml:id="mdfe-001-a-1460">desiring</w>
          <w lemma="you" pos="pn" reg="you" xml:id="mdfe-001-a-1470">you</w>
          <w lemma="not" pos="xx" reg="not" xml:id="mdfe-001-a-1480">not</w>
          <w lemma="only" pos="av_j" reg="only" xml:id="mdfe-001-a-1490">onely</w>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="mdfe-001-a-1500">to</w>
          <w lemma="extend" pos="vvi" reg="extend" xml:id="mdfe-001-a-1510">extend</w>
          <w lemma="your" pos="po" reg="your" xml:id="mdfe-001-a-1520">your</w>
          <w lemma="own" pos="d" reg="own" xml:id="mdfe-001-a-1530">owne</w>
          <w lemma="charitable" pos="j" reg="charitable" xml:id="mdfe-001-a-1540">charitable</w>
          <w lemma="contribution" pos="n1" reg="contribution" xml:id="mdfe-001-a-1550">contribution</w>
          <w lemma="thereunto" pos="av" reg="thereunto" xml:id="mdfe-001-a-1560">thereunto</w>
          <pc xml:id="mdfe-001-a-1570">,</pc>
          <w lemma="but" pos="acp-cc" reg="but" xml:id="mdfe-001-a-1580">but</w>
          <w lemma="also" pos="av" reg="also" xml:id="mdfe-001-a-1590">also</w>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="mdfe-001-a-1600">to</w>
          <w lemma="move" pos="vvi" reg="move" xml:id="mdfe-001-a-1610">mooue</w>
          <w lemma="and" pos="cc" reg="and" xml:id="mdfe-001-a-1620">and</w>
          <w lemma="exhort" pos="vvi" reg="exhort" xml:id="mdfe-001-a-1630">exhort</w>
          <w lemma="all" pos="d" reg="all" xml:id="mdfe-001-a-1640">all</w>
          <w lemma="those" pos="d" reg="those" xml:id="mdfe-001-a-1650">those</w>
          <w lemma="within" pos="acp-p" reg="within" xml:id="mdfe-001-a-1660">within</w>
          <w lemma="your" pos="po" reg="your" xml:id="mdfe-001-a-1670">your</w>
          <w lemma="several" pos="j" reg="several" xml:id="mdfe-001-a-1680">seuerall</w>
          <w lemma="city" pos="n2" reg="Cities" xml:id="mdfe-001-a-1690">Citties</w>
          <pc xml:id="mdfe-001-a-1700">,</pc>
          <w lemma="town" pos="n2" reg="Towns" xml:id="mdfe-001-a-1710">Townes</w>
          <pc xml:id="mdfe-001-a-1720">,</pc>
          <w lemma="port" pos="n2" reg="Ports" xml:id="mdfe-001-a-1730">Ports</w>
          <pc xml:id="mdfe-001-a-1740">,</pc>
          <w lemma="village" pos="n2" reg="Villages" xml:id="mdfe-001-a-1750">Villages</w>
          <pc xml:id="mdfe-001-a-1760">,</pc>
          <w lemma="and" pos="cc" reg="and" xml:id="mdfe-001-a-1770">and</w>
          <w lemma="parish" pos="n2" reg="Parishes" xml:id="mdfe-001-a-1780">Parishes</w>
          <pc xml:id="mdfe-001-a-1790">,</pc>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="mdfe-001-a-1800">to</w>
          <w lemma="contribute" pos="vvi" reg="contribute" xml:id="mdfe-001-a-1810">contribute</w>
          <w lemma="cheerful" pos="av_j" reg="cheerfully" xml:id="mdfe-001-a-1820">cheerefully</w>
          <w lemma="and" pos="cc" reg="and" xml:id="mdfe-001-a-1830">and</w>
          <w lemma="liberal" pos="av_j" reg="liberally" xml:id="mdfe-001-a-1840">liberally</w>
          <w lemma="towards" pos="acp-p" reg="toward" xml:id="mdfe-001-a-1850">toward</w>
          <w lemma="the" pos="d" reg="the" xml:id="mdfe-001-a-1860">the</w>
          <w lemma="effect" pos="n1_vg" reg="effecting" xml:id="mdfe-001-a-1870">effecting</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-1880">of</w>
          <w lemma="a" pos="d" reg="a" xml:id="mdfe-001-a-1890">a</w>
          <w lemma="work" pos="n1" reg="work" xml:id="mdfe-001-a-1900">worke</w>
          <w lemma="so" pos="av" reg="so" xml:id="mdfe-001-a-1910">so</w>
          <w lemma="profitable" pos="j" reg="profitable" xml:id="mdfe-001-a-1920">profitable</w>
          <w lemma="to" pos="acp-p" reg="to" xml:id="mdfe-001-a-1930">to</w>
          <w lemma="the" pos="d" reg="the" xml:id="mdfe-001-a-1940">the</w>
          <w lemma="common" pos="j" reg="Common" xml:id="mdfe-001-a-1950">Common</w>
          <w lemma="wealth" pos="n1" reg="wealth" xml:id="mdfe-001-a-1960">wealth</w>
          <pc xml:id="mdfe-001-a-1970">,</pc>
          <w lemma="and" pos="cc" reg="and" xml:id="mdfe-001-a-1980">and</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-1990">of</w>
          <w lemma="so" pos="av" reg="so" xml:id="mdfe-001-a-2000">so</w>
          <w lemma="great" pos="j" reg="great" xml:id="mdfe-001-a-2010">great</w>
          <w lemma="a" pos="d" reg="a" xml:id="mdfe-001-a-2020">a</w>
          <w lemma="consequence" pos="n1" reg="consequence" xml:id="mdfe-001-a-2030">consequence</w>
          <w lemma="for" pos="acp-p" reg="for" xml:id="mdfe-001-a-2040">for</w>
          <w lemma="the" pos="d" reg="the" xml:id="mdfe-001-a-2050">the</w>
          <w lemma="safety" pos="n1" reg="safety" xml:id="mdfe-001-a-2060">safetie</w>
          <w lemma="and" pos="cc" reg="and" xml:id="mdfe-001-a-2070">and</w>
          <w lemma="defence" pos="n1" reg="defence" xml:id="mdfe-001-a-2080">defence</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-2090">of</w>
          <w lemma="the" pos="d" reg="the" xml:id="mdfe-001-a-2100">the</w>
          <w lemma="coast" pos="n1" reg="Coast" xml:id="mdfe-001-a-2110">Coast</w>
          <pc unit="sentence" xml:id="mdfe-001-a-2120">.</pc>
          <w lemma="and" pos="cc" reg="And" xml:id="mdfe-001-a-2130">And</w>
          <w lemma="so" pos="av" reg="so" xml:id="mdfe-001-a-2140">so</w>
          <w lemma="commend" pos="vvg" reg="commending" xml:id="mdfe-001-a-2150">commending</w>
          <w lemma="this" pos="d" reg="this" xml:id="mdfe-001-a-2160">this</w>
          <w lemma="same" pos="d" reg="same" xml:id="mdfe-001-a-2170">same</w>
          <w lemma="to" pos="acp-p" reg="to" xml:id="mdfe-001-a-2180">to</w>
          <w lemma="your" pos="po" reg="your" xml:id="mdfe-001-a-2190">your</w>
          <w lemma="best" pos="j-s" reg="best" xml:id="mdfe-001-a-2200">best</w>
          <w lemma="care" pos="n1" reg="care" xml:id="mdfe-001-a-2210">care</w>
          <w lemma="and" pos="cc" reg="and" xml:id="mdfe-001-a-2220">and</w>
          <w lemma="discreet" pos="j" reg="discreet" xml:id="mdfe-001-a-2230">discreet</w>
          <w lemma="carriage" pos="n1" reg="carriage" xml:id="mdfe-001-a-2240">carriage</w>
          <pc xml:id="mdfe-001-a-2250">,</pc>
          <w lemma="i" pos="pns" reg="I" xml:id="mdfe-001-a-2260">I</w>
          <w lemma="bid" pos="vvb" reg="bid" xml:id="mdfe-001-a-2270">bid</w>
          <w lemma="you" pos="pn" reg="you" xml:id="mdfe-001-a-2280">you</w>
          <w lemma="farewell" pos="n1" reg="farewell" xml:id="mdfe-001-a-2290">farewell</w>
          <pc unit="sentence" xml:id="mdfe-001-a-2300">.</pc>
          <w lemma="from" pos="acp-p" reg="From" xml:id="mdfe-001-a-2310">From</w>
          <w lemma="my" pos="po" reg="my" xml:id="mdfe-001-a-2320">my</w>
          <w lemma="house" pos="n1" reg="house" xml:id="mdfe-001-a-2330">house</w>
          <w lemma="in" pos="acp-p" reg="in" xml:id="mdfe-001-a-2340">in</w>
          <w lemma="phillip-lane" pos="n1" reg="Phillip-lane" xml:id="mdfe-001-a-2350">Phillip-lane</w>
          <pc xml:id="mdfe-001-a-2360">,</pc>
          <hi xml:id="mdfe-e80">
            <w lemma="London" pos="n1-nn" reg="London" xml:id="mdfe-001-a-2370">London</w>
            <pc xml:id="mdfe-001-a-2380">,</pc>
          </hi>
          <w lemma="this" pos="d" reg="this" xml:id="mdfe-001-a-2390">this</w>
          <w lemma="19" pos="crd" reg="19" xml:id="mdfe-001-a-2400">19.</w>
          <w lemma="day" pos="n1" reg="day" xml:id="mdfe-001-a-2410">day</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-2420">of</w>
          <w lemma="june" pos="n1-nn" reg="june" xml:id="mdfe-001-a-2430">Iune</w>
          <pc xml:id="mdfe-001-a-2440">,</pc>
          <w lemma="1619." pos="crd" reg="1619." xml:id="mdfe-001-a-2450">1619.</w>
          <pc unit="sentence" xml:id="mdfe-001-a-2460"/>
        </p>
        <closer xml:id="mdfe-e90">
          <signed xml:id="mdfe-e100">
            <w lemma="your" pos="po" reg="Your" xml:id="mdfe-001-a-2470">Your</w>
            <w lemma="very" pos="j" reg="very" xml:id="mdfe-001-a-2480">very</w>
            <w lemma="love" pos="j_vg" reg="loving" xml:id="mdfe-001-a-2490">louing</w>
            <w lemma="friend" pos="n1" reg="Friend" xml:id="mdfe-001-a-2500">Friend</w>
            <pc xml:id="mdfe-001-a-2510">,</pc>
            <w lemma="e." pos="n-ab" reg="E." xml:id="mdfe-001-a-2520">E.</w>
            <w lemma="zouch" pos="av_d" reg="ZOUCH" xml:id="mdfe-001-a-2530">ZOVCH</w>
            <pc unit="sentence" xml:id="mdfe-001-a-2540">.</pc>
          </signed>
        </closer>
        <postscript xml:id="mdfe-e110">
          <p xml:id="mdfe-e120">
            <w lemma="to" pos="acp-cs" reg="To" xml:id="mdfe-001-a-2550">To</w>
            <w lemma="all" pos="d" reg="all" xml:id="mdfe-001-a-2560">all</w>
            <w lemma="mayor" pos="n2" reg="Mayors" xml:id="mdfe-001-a-2570">Mayors</w>
            <pc xml:id="mdfe-001-a-2580">,</pc>
            <w lemma="jurate" pos="n2" reg="jurates" xml:id="mdfe-001-a-2590">Iurates</w>
            <pc xml:id="mdfe-001-a-2600">,</pc>
            <w lemma="constable" pos="n2" reg="Constables" xml:id="mdfe-001-a-2610">Constables</w>
            <pc xml:id="mdfe-001-a-2620">,</pc>
            <w lemma="bailiff" pos="n2" reg="Bailiffs" xml:id="mdfe-001-a-2630">Bayliffes</w>
            <pc xml:id="mdfe-001-a-2640">,</pc>
            <w lemma="minister" pos="n2" reg="Ministers" xml:id="mdfe-001-a-2650">Ministers</w>
            <pc xml:id="mdfe-001-a-2660">,</pc>
            <w lemma="churchwarden" pos="n2" reg="Churchwardens" xml:id="mdfe-001-a-2670">Churchwardens</w>
            <pc xml:id="mdfe-001-a-2680">,</pc>
            <w lemma="and" pos="cc" reg="and" xml:id="mdfe-001-a-2690">and</w>
            <w lemma="to" pos="acp-p" reg="to" xml:id="mdfe-001-a-2700">to</w>
            <w lemma="all" pos="d" reg="all" xml:id="mdfe-001-a-2710">all</w>
            <w lemma="other" pos="pi_d" reg="other" xml:id="mdfe-001-a-2720">other</w>
            <w lemma="his" pos="po" reg="his" xml:id="mdfe-001-a-2730">his</w>
            <w lemma="majesty" pos="n1g" reg="Majesty's" xml:id="mdfe-001-a-2740">Maiesties</w>
            <w lemma="officer" pos="n2" reg="Officers" xml:id="mdfe-001-a-2750">Officers</w>
            <w lemma="within" pos="acp-p" reg="within" xml:id="mdfe-001-a-2760">within</w>
            <w lemma="the" pos="d" reg="the" xml:id="mdfe-001-a-2770">the</w>
            <w lemma="liberty" pos="n1" reg="Liberty" xml:id="mdfe-001-a-2780">Libertie</w>
            <w lemma="of" pos="acp-p" reg="of" xml:id="mdfe-001-a-2790">of</w>
            <w lemma="the" pos="d" reg="the" xml:id="mdfe-001-a-2800">the</w>
            <w lemma="cinque" pos="fw-fr" reg="Cinque" xml:id="mdfe-001-a-2810">Cinque</w>
            <w lemma="port" pos="n2" reg="Ports" xml:id="mdfe-001-a-2820">Ports</w>
            <pc xml:id="mdfe-001-a-2830">,</pc>
            <w lemma="as" pos="acp-cs" reg="as" xml:id="mdfe-001-a-2840">as</w>
            <w lemma="it" pos="pn" reg="it" xml:id="mdfe-001-a-2850">it</w>
            <w lemma="shall" pos="vmb" reg="shall" xml:id="mdfe-001-a-2860">shall</w>
            <w lemma="appertain" pos="vvi" reg="appertain" xml:id="mdfe-001-a-2870">appertaine</w>
            <pc unit="sentence" xml:id="mdfe-001-a-2880">.</pc>
          </p>
        </postscript>
      </div>
    </body>
    <back xml:id="mdfe-t-b"/>
  </text>
</TEI>

