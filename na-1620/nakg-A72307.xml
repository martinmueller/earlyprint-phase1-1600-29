<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>The workes of Mr Sam. Hieron late pastor of Modbury in Deuon· The second volume</title>
        <author>Hieron, Samuel, 1576?-1617.</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1620</date>
        </edition>
      </editionStmt>
      <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2009-10 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A72307</idno>
        <idno type="STC">STC 13384.3</idno>
        <idno type="STC">STC 13381</idno>
        <idno type="STC">STC 13380</idno>
        <idno type="STC">ESTC S119610</idno>
        <idno type="EEBO-CITATION">99900300</idno>
        <idno type="PROQUEST">99900300</idno>
        <idno type="VID">178016</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A72307)</note>
        <note>Transcribed from: (Early English Books Online ; image set 178016)</note>
        <note>Images scanned from microfilm: (Early English books, 1475-1640 ; 1352:3a; 1575:3a; 1991:1)</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>The workes of Mr Sam. Hieron late pastor of Modbury in Deuon· The second volume</title>
            <author>Hieron, Samuel, 1576?-1617.</author>
            <author>Hill, Robert, d. 1623.</author>
          </titleStmt>
          <extent>[24], 500, [12] p.</extent>
          <publicationStmt>
            <publisher>printed by Wi: Stansby and are to be sold by I. Parker,</publisher>
            <pubPlace>London :</pubPlace>
            <date>[1620]</date>
          </publicationStmt>
          <notesStmt>
            <note>Editor's note "To the reader" signed: Robert Hill.</note>
            <note>The title page is engraved and signed "R: Elstracke scuplsit·". When it occurs in other contexts it is separately catalogued as STC 13377.5, but here it is printed as [par.]2.</note>
            <note>The first leaf is blank.</note>
            <note>"The doctrines triall", "The Christians liue-loode", "Penance for sinne", "Foure learned and godly sermons", "A present for Cæsar. The second sermon", "A bargaine of salt", and "A bargaine of salt. The second sermon" each have separate title page, all but the first and last dated 1619; pagination and register are continuous.</note>
            <note>Includes index.</note>
            <note>Identified as vol. 2 of STC 13381 on UMI microfilm reel 1352, and as vol. 2 of STC 13380 on reel 1575.</note>
            <note>Reproduction of original in the Henry E. Huntington Library and Art Gallery, San Marino, California.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
    </profileDesc>
    <revisionDesc>
      <change><date>2008-08</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2008-10</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2008-12</date><label>John Pas</label>
        Sampled and proofread
      </change>
      <change><date>2008-12</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="nakg-t">
    <body xml:id="nakg-e0">
      <div type="title_page" xml:id="nakg-e10">
        <pb facs="tcp:178016:1" rendition="simple:additions" xml:id="nakg-001-a"/>
        <figure xml:id="nakg-e20">
          <p xml:id="nakg-e30">
            <w lemma="s" pos="sy" reg="S" xml:id="nakg-001-a-0010">S</w>
            <pc xml:id="nakg-001-a-0020">:</pc>
            <w lemma="MATTHEW" pos="n1-nn" reg="MATTHEW" xml:id="nakg-001-a-0030">MATTHEW</w>
          </p>
        </figure>
        <figure xml:id="nakg-e40">
          <figDesc xml:id="nakg-e50">depiction of king kneeling before tetragramaton</figDesc>
          <p xml:id="nakg-e60">
            <w lemma="יהוה" pos="n1" reg="יהוה" xml:id="nakg-001-a-0040">יהוה</w>
          </p>
        </figure>
        <figure xml:id="nakg-e70">
          <p xml:id="nakg-e80">
            <w lemma="s" pos="sy" reg="S" xml:id="nakg-001-a-0050">S</w>
            <pc xml:id="nakg-001-a-0060">:</pc>
            <w lemma="Mark" pos="n1-nn" reg="MARK" xml:id="nakg-001-a-0070">MARKE</w>
          </p>
        </figure>
        <figure xml:id="nakg-e90">
          <p xml:id="nakg-e100">
            <w lemma="humility" pos="n1" reg="HUMILITY" xml:id="nakg-001-a-0080">HUMILITIE</w>
          </p>
        </figure>
        <p xml:id="nakg-e110">
          <w lemma="the" pos="d" reg="THE" xml:id="nakg-001-a-0090">THE</w>
          <w lemma="work" pos="n2" reg="WORKS" xml:id="nakg-001-a-0100">WORKES</w>
          <w lemma="of" pos="acp-p" reg="OF" xml:id="nakg-001-a-0110">OF</w>
          <w lemma="m" pos="n1" reg="M" xml:id="nakg-001-a-0120">M</w>
          <hi rendition="#sup" xml:id="nakg-e120">
            <w lemma="r" pos="sy" reg="R" xml:id="nakg-001-a-0130">R</w>
          </hi>
          <w lemma="SAM" pos="n1-nn" reg="SAM" xml:id="nakg-001-a-0140">SAM</w>
          <pc unit="sentence" xml:id="nakg-001-a-0150">.</pc>
          <w lemma="Hieron" pos="n1-nn" reg="HIERON" xml:id="nakg-001-a-0160">HIERON</w>
          <w lemma="late" pos="j" reg="late" xml:id="nakg-001-a-0170">late</w>
          <w lemma="pastor" pos="n1" reg="Pastor" xml:id="nakg-001-a-0180">Pastor</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="nakg-001-a-0190">of</w>
          <w lemma="modbury" pos="n1-nn" reg="Modbury" xml:id="nakg-001-a-0200">Modbury</w>
          <w lemma="in" pos="acp-p" reg="in" xml:id="nakg-001-a-0210">in</w>
          <w lemma="Devon" pos="n1-nn" reg="DEVON" xml:id="nakg-001-a-0220">DEUON</w>
          <pc unit="sentence" xml:id="nakg-001-a-0230">.</pc>
        </p>
        <q xml:id="nakg-e130">
          <bibl xml:id="nakg-e140">
            <w lemma="JOHN" pos="n1-nn" reg="JOHN" xml:id="nakg-001-a-0240">IOHN</w>
            <pc unit="sentence" xml:id="nakg-001-a-0250">.</pc>
            <w lemma="5.55" pos="crd" reg="5.55" xml:id="nakg-001-a-0260">5.55</w>
            <pc unit="sentence" xml:id="nakg-001-a-0270">.</pc>
          </bibl>
          <hi xml:id="nakg-e150">
            <w lemma="he" pos="pns" reg="He" xml:id="nakg-001-a-0280">He</w>
            <w lemma="be" pos="vvd" reg="was" xml:id="nakg-001-a-0290">was</w>
            <w lemma="a" pos="d" reg="a" xml:id="nakg-001-a-0300">a</w>
            <w lemma="burn" pos="j_vg" reg="burning" xml:id="nakg-001-a-0310">burning</w>
            <w lemma="and" pos="cc" reg="and" xml:id="nakg-001-a-0320">and</w>
            <w lemma="a" pos="d" reg="a" xml:id="nakg-001-a-0330">a</w>
            <w lemma="shine" pos="j_vg" reg="shining" xml:id="nakg-001-a-0340">shining</w>
            <w lemma="light" pos="n1" reg="light" xml:id="nakg-001-a-0350">light</w>
            <pc unit="sentence" xml:id="nakg-001-a-0360">.</pc>
          </hi>
        </q>
        <figure xml:id="nakg-e160">
          <p xml:id="nakg-e170">
            <w lemma="faith" pos="n1" reg="FAITH" xml:id="nakg-001-a-0370">FAITH</w>
          </p>
        </figure>
        <figure xml:id="nakg-e180">
          <p xml:id="nakg-e190">
            <w lemma="s" pos="sy" reg="S" xml:id="nakg-001-a-0380">S</w>
            <pc xml:id="nakg-001-a-0390">:</pc>
            <w lemma="Luke" pos="n1-nn" reg="LUKE" xml:id="nakg-001-a-0400">LUKE</w>
          </p>
        </figure>
        <p xml:id="nakg-e200">
          <w lemma="LONDON" pos="n1-nn" reg="LONDON" xml:id="nakg-001-a-0410">LONDON</w>
          <hi xml:id="nakg-e210">
            <w lemma="print" pos="vvn" reg="Printed" xml:id="nakg-001-a-0420">Printed</w>
            <w lemma="by" pos="acp-p" reg="by" xml:id="nakg-001-a-0430">by</w>
            <w lemma="William" pos="n1-nn" reg="William" xml:id="nakg-001-a-0440">William</w>
            <w lemma="Stansby" pos="n1-nn" reg="Stansby" xml:id="nakg-001-a-0450">Stansby</w>
            <w lemma="and" pos="cc" reg="and" xml:id="nakg-001-a-0460">and</w>
            <w lemma="John" pos="n1-nn" reg="john" xml:id="nakg-001-a-0470">Iohn</w>
            <w lemma="beale" pos="n1-nn" reg="Beale" xml:id="nakg-001-a-0480">Beale</w>
          </hi>
        </p>
        <figure xml:id="nakg-e220">
          <p xml:id="nakg-e230">
            <w lemma="saint" pos="n1" reg="SAINT" xml:id="nakg-001-a-0490">S</w>
            <pc xml:id="nakg-001-a-0500">:</pc>
            <w lemma="JOHN" pos="n1-nn" reg="JOHN" xml:id="nakg-001-a-0510">IOHN</w>
          </p>
        </figure>
      </div>
    </body>
    <back xml:id="nakg-t-b"/>
  </text>
</TEI>

