<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>By the King a proclamation for remouing the receipt of His Maiesties exchequer from Westminster to Richmond.</title>
        <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1625</date>
        </edition>
      </editionStmt>
      <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2007-01 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A22378</idno>
        <idno type="STC">STC 8789</idno>
        <idno type="STC">ESTC S123752</idno>
        <idno type="EEBO-CITATION">33150353</idno>
        <idno type="OCLC">ocm 33150353</idno>
        <idno type="VID">28562</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A22378)</note>
        <note>Transcribed from: (Early English Books Online ; image set 28562)</note>
        <note>Images scanned from microfilm: (Early English books, 1475-1640 ; 1876:49)</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>By the King a proclamation for remouing the receipt of His Maiesties exchequer from Westminster to Richmond.</title>
            <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
            <author>Charles I, King of England, 1600-1649.</author>
          </titleStmt>
          <extent>1 sheet ([1] p.).</extent>
          <publicationStmt>
            <publisher>By I.L. and W.T. for Bonham Norton and Iohn Bill, Printers to the Kings most Excellent Maiestie,</publisher>
            <pubPlace>Printed at Oxford :</pubPlace>
            <date>1625.</date>
          </publicationStmt>
          <notesStmt>
            <note>line 1 of text ends "conside-".</note>
            <note>"Giuen at the Court at Ricot the one and thirtieth day of Iulie, in the first yeare of his Maiesties Raigne of great Brittaine, France and Ireland."</note>
            <note>Reproduction of original in: Harvard University. Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>England and Wales. -- Exchequer.</term>
          <term>Plague -- England -- Early works to 1800.</term>
          <term>Great Britain -- History -- Charles I, 1625-1649.</term>
          <term>Great Britain -- Politics and government -- 1625-1649.</term>
          <term>Broadsides -- London (England) -- 17th century.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2006-03</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2006-05</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2006-06</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
      <change><date>2006-06</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2006-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="ncqx-t">
    <body xml:id="ncqx-e0">
      <div type="royal_proclamation" xml:id="ncqx-e10">
        <pb facs="tcp:28562:1" xml:id="ncqx-001-a"/>
        <head xml:id="ncqx-e20">
          <w lemma="by" pos="acp-p" reg="BY" xml:id="ncqx-001-a-0010">BY</w>
          <w lemma="the" pos="d" reg="THE" xml:id="ncqx-001-a-0020">THE</w>
          <w lemma="king" pos="n1" reg="KING" xml:id="ncqx-001-a-0030">KING</w>
          <pc unit="sentence" xml:id="ncqx-001-a-0040">.</pc>
          <lb xml:id="ncqx-e30"/>
          <w lemma="a" pos="d" reg="A" xml:id="ncqx-001-a-0050">A</w>
          <w lemma="proclamation" pos="n1" reg="Proclamation" xml:id="ncqx-001-a-0060">Proclamation</w>
          <w lemma="for" pos="acp-p" reg="for" xml:id="ncqx-001-a-0070">for</w>
          <w lemma="remove" pos="vvg" reg="removing" xml:id="ncqx-001-a-0080">remouing</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-0090">the</w>
          <w lemma="receipt" pos="n1" reg="Receipt" xml:id="ncqx-001-a-0100">Receipt</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="ncqx-001-a-0110">of</w>
          <w lemma="his" pos="po" reg="his" xml:id="ncqx-001-a-0120">his</w>
          <w lemma="majesty" pos="n1g" reg="Majesty's" xml:id="ncqx-001-a-0130">Maiesties</w>
          <w lemma="exchequer" pos="n1" reg="Exchequer" xml:id="ncqx-001-a-0140">Exchequer</w>
          <w lemma="from" pos="acp-p" reg="from" xml:id="ncqx-001-a-0150">from</w>
          <w lemma="Westminster" pos="n1-nn" reg="Westminster" xml:id="ncqx-001-a-0160">Westminster</w>
          <w lemma="to" pos="acp-p" reg="to" xml:id="ncqx-001-a-0170">to</w>
          <w lemma="Richmond" pos="n1-nn" reg="Richmond" xml:id="ncqx-001-a-0180">Richmond</w>
          <pc unit="sentence" xml:id="ncqx-001-a-0190">.</pc>
        </head>
        <p xml:id="ncqx-e40">
          <w lemma="the" pos="d" reg="THE" rendition="#decorinit" xml:id="ncqx-001-a-0200">THE</w>
          <w lemma="king" pos="n2" reg="Kings" xml:id="ncqx-001-a-0210">Kings</w>
          <w lemma="most" pos="av-s_d" reg="most" xml:id="ncqx-001-a-0220">most</w>
          <w lemma="excellent" pos="j" reg="Excellent" xml:id="ncqx-001-a-0230">Excellent</w>
          <w lemma="majesty" pos="n1" reg="Majesty" xml:id="ncqx-001-a-0240">Maiestie</w>
          <w lemma="take" pos="vvg" reg="taking" xml:id="ncqx-001-a-0250">taking</w>
          <w lemma="into" pos="acp-p" reg="into" xml:id="ncqx-001-a-0260">into</w>
          <w lemma="his" pos="po" reg="his" xml:id="ncqx-001-a-0270">his</w>
          <w lemma="princely" pos="j" reg="Princely" xml:id="ncqx-001-a-0280">Princely</w>
          <w lemma="consideration" pos="n1" reg="consideration" xml:id="ncqx-001-a-0290">consideration</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-0300">the</w>
          <w lemma="great" pos="j" reg="great" xml:id="ncqx-001-a-0310">great</w>
          <w lemma="and" pos="cc" reg="and" xml:id="ncqx-001-a-0320">and</w>
          <w lemma="dangerous" pos="j" reg="dangerous" xml:id="ncqx-001-a-0330">dangerous</w>
          <w lemma="increase" pos="n1" reg="increase" xml:id="ncqx-001-a-0340">increase</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="ncqx-001-a-0350">of</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-0360">the</w>
          <w lemma="plague" pos="n1" reg="Plague" xml:id="ncqx-001-a-0370">Plague</w>
          <w lemma="in" pos="acp-p" reg="in" xml:id="ncqx-001-a-0380">in</w>
          <w lemma="and" pos="cc" reg="and" xml:id="ncqx-001-a-0390">and</w>
          <w lemma="about" pos="acp-av" reg="about" xml:id="ncqx-001-a-0400">about</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-0410">the</w>
          <w lemma="city" pos="n1" reg="City" xml:id="ncqx-001-a-0420">Citty</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="ncqx-001-a-0430">of</w>
          <w lemma="Westminster" pos="n1-nn" reg="Westminster" xml:id="ncqx-001-a-0440">Westminster</w>
          <pc xml:id="ncqx-001-a-0450">,</pc>
          <w lemma="where" pos="crq-cs" reg="where" xml:id="ncqx-001-a-0460">where</w>
          <w lemma="his" pos="po" reg="his" xml:id="ncqx-001-a-0470">his</w>
          <w lemma="majesty" pos="n1g" reg="Majesty's" xml:id="ncqx-001-a-0480">Maiesties</w>
          <w lemma="receipt" pos="n1" reg="Receipt" xml:id="ncqx-001-a-0490">Receit</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="ncqx-001-a-0500">of</w>
          <w lemma="exchequer" pos="n1" reg="Exchequer" xml:id="ncqx-001-a-0510">Exchequer</w>
          <w lemma="have" pos="vvz" reg="hath" xml:id="ncqx-001-a-0520">hath</w>
          <w lemma="be" pos="vvn" reg="been" xml:id="ncqx-001-a-0530">beene</w>
          <w lemma="hitherto" pos="av" reg="hitherto" xml:id="ncqx-001-a-0540">hitherto</w>
          <w lemma="keep" pos="vvn" reg="kept" xml:id="ncqx-001-a-0550">kept</w>
          <pc xml:id="ncqx-001-a-0560">,</pc>
          <w lemma="and" pos="cc" reg="and" xml:id="ncqx-001-a-0570">and</w>
          <w lemma="willing" pos="j" reg="willing" xml:id="ncqx-001-a-0580">willing</w>
          <w lemma="as" pos="acp-cs" reg="as" xml:id="ncqx-001-a-0590">as</w>
          <w lemma="much" pos="d" reg="much" xml:id="ncqx-001-a-0600">much</w>
          <w lemma="as" pos="acp-cs" reg="as" xml:id="ncqx-001-a-0610">as</w>
          <w lemma="be" pos="vvz" reg="is" xml:id="ncqx-001-a-0620">is</w>
          <w lemma="possible" pos="j" reg="possible" xml:id="ncqx-001-a-0630">possible</w>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="ncqx-001-a-0640">to</w>
          <w lemma="prevent" pos="vvi" reg="prevent" xml:id="ncqx-001-a-0650">prevent</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-0660">the</w>
          <w lemma="further" pos="j-c" reg="further" xml:id="ncqx-001-a-0670">further</w>
          <w lemma="danger" pos="n1" reg="danger" xml:id="ncqx-001-a-0680">danger</w>
          <pc xml:id="ncqx-001-a-0690">,</pc>
          <w lemma="which" pos="crq-r" reg="which" xml:id="ncqx-001-a-0700">which</w>
          <w lemma="may" pos="vmd" reg="might" xml:id="ncqx-001-a-0710">might</w>
          <w lemma="ensue" pos="vvi" reg="ensue" xml:id="ncqx-001-a-0720">ensue</w>
          <w lemma="as" pos="acp-cs" reg="as" xml:id="ncqx-001-a-0730">as</w>
          <w lemma="well" pos="av" reg="well" xml:id="ncqx-001-a-0740">well</w>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="ncqx-001-a-0750">to</w>
          <w lemma="his" pos="po" reg="his" xml:id="ncqx-001-a-0760">his</w>
          <w lemma="own" pos="d" reg="own" xml:id="ncqx-001-a-0770">owne</w>
          <w lemma="officer" pos="n2" reg="Officers" xml:id="ncqx-001-a-0780">Officers</w>
          <pc xml:id="ncqx-001-a-0790">,</pc>
          <w lemma="which" pos="crq-r" reg="which" xml:id="ncqx-001-a-0800">which</w>
          <w lemma="be" pos="vvb" reg="are" xml:id="ncqx-001-a-0810">are</w>
          <w lemma="necessary" pos="av_j" reg="necessarily" xml:id="ncqx-001-a-0820">necessarily</w>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="ncqx-001-a-0830">to</w>
          <w lemma="attend" pos="vvi" reg="attend" xml:id="ncqx-001-a-0840">attend</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-0850">the</w>
          <w lemma="same" pos="d" reg="same" xml:id="ncqx-001-a-0860">same</w>
          <w lemma="receipt" pos="n1" reg="Receipt" xml:id="ncqx-001-a-0870">Receit</w>
          <pc xml:id="ncqx-001-a-0880">,</pc>
          <w lemma="as" pos="acp-cs" reg="as" xml:id="ncqx-001-a-0890">as</w>
          <w lemma="to" pos="acp-p" reg="to" xml:id="ncqx-001-a-0900">to</w>
          <w lemma="other" pos="d" reg="other" xml:id="ncqx-001-a-0910">other</w>
          <w lemma="his" pos="po" reg="his" xml:id="ncqx-001-a-0920">his</w>
          <w lemma="love" pos="j_vg" reg="loving" xml:id="ncqx-001-a-0930">louing</w>
          <w lemma="subject" pos="n2" reg="Subjects" xml:id="ncqx-001-a-0940">Subiects</w>
          <w lemma="who" pos="crq-r" reg="who" xml:id="ncqx-001-a-0950">who</w>
          <w lemma="shall" pos="vmb" reg="shall" xml:id="ncqx-001-a-0960">shall</w>
          <w lemma="have" pos="vvi" reg="have" xml:id="ncqx-001-a-0970">haue</w>
          <w lemma="occasion" pos="n1" reg="occasion" xml:id="ncqx-001-a-0980">occasion</w>
          <w lemma="either" pos="av_d" reg="either" xml:id="ncqx-001-a-0990">either</w>
          <w lemma="for" pos="acp-p" reg="for" xml:id="ncqx-001-a-1000">for</w>
          <w lemma="receipt" pos="n1" reg="receipt" xml:id="ncqx-001-a-1010">receit</w>
          <pc xml:id="ncqx-001-a-1020">,</pc>
          <w lemma="or" pos="cc" reg="or" xml:id="ncqx-001-a-1030">or</w>
          <w lemma="payment" pos="n1" reg="payment" xml:id="ncqx-001-a-1040">payment</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="ncqx-001-a-1050">of</w>
          <w lemma="money" pos="n2" reg="monies" xml:id="ncqx-001-a-1060">monies</w>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="ncqx-001-a-1070">to</w>
          <w lemma="repair" pos="vvi" reg="repair" xml:id="ncqx-001-a-1080">repaire</w>
          <w lemma="thither" pos="av" reg="thither" xml:id="ncqx-001-a-1090">thither</w>
          <pc xml:id="ncqx-001-a-1100">:</pc>
          <w lemma="have" pos="vvz" reg="hath" xml:id="ncqx-001-a-1110">hath</w>
          <w lemma="therefore" pos="av" reg="therefore" xml:id="ncqx-001-a-1120">therefore</w>
          <w lemma="take" pos="vvn" reg="taken" xml:id="ncqx-001-a-1130">taken</w>
          <w lemma="order" pos="n1" reg="order" xml:id="ncqx-001-a-1140">order</w>
          <w lemma="for" pos="acp-p" reg="for" xml:id="ncqx-001-a-1150">for</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-1160">the</w>
          <w lemma="present" pos="j" reg="present" xml:id="ncqx-001-a-1170">present</w>
          <w lemma="remove" pos="n1" reg="remove" xml:id="ncqx-001-a-1180">remoue</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="ncqx-001-a-1190">of</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-1200">the</w>
          <w lemma="receipt" pos="n1" reg="receipt" xml:id="ncqx-001-a-1210">receit</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="ncqx-001-a-1220">of</w>
          <w lemma="his" pos="po" reg="his" xml:id="ncqx-001-a-1230">his</w>
          <w lemma="say" pos="j_vn" reg="said" xml:id="ncqx-001-a-1240">said</w>
          <w lemma="exchequer" pos="n1" reg="Exchequer" xml:id="ncqx-001-a-1250">Exchequer</w>
          <w lemma="from" pos="acp-p" reg="from" xml:id="ncqx-001-a-1260">from</w>
          <w lemma="thence" pos="av" reg="thence" xml:id="ncqx-001-a-1270">thence</w>
          <w lemma="to" pos="acp-p" reg="to" xml:id="ncqx-001-a-1280">to</w>
          <w lemma="his" pos="po" reg="his" xml:id="ncqx-001-a-1290">his</w>
          <w lemma="majesty" pos="n1g" reg="Majesty's" xml:id="ncqx-001-a-1300">Maiesties</w>
          <w lemma="house" pos="n1" reg="house" xml:id="ncqx-001-a-1310">house</w>
          <w lemma="at" pos="acp-p" reg="at" xml:id="ncqx-001-a-1320">at</w>
          <w lemma="Richmond" pos="n1-nn" reg="Richmond" xml:id="ncqx-001-a-1330">Richmond</w>
          <w lemma="in" pos="acp-p" reg="in" xml:id="ncqx-001-a-1340">in</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-1350">the</w>
          <w lemma="county" pos="n1" reg="County" xml:id="ncqx-001-a-1360">Countie</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="ncqx-001-a-1370">of</w>
          <w lemma="Surrey" pos="n1-nn" reg="Surrey" xml:id="ncqx-001-a-1380">Surrey</w>
          <pc xml:id="ncqx-001-a-1390">:</pc>
          <w lemma="and" pos="cc" reg="and" xml:id="ncqx-001-a-1400">and</w>
          <w lemma="have" pos="vvz" reg="hath" xml:id="ncqx-001-a-1410">hath</w>
          <w lemma="think" pos="vvn" reg="thought" xml:id="ncqx-001-a-1420">thought</w>
          <w lemma="fit" pos="j" reg="fit" xml:id="ncqx-001-a-1430">fit</w>
          <w lemma="by" pos="acp-p" reg="by" xml:id="ncqx-001-a-1440">by</w>
          <w lemma="this" pos="d" reg="this" xml:id="ncqx-001-a-1450">this</w>
          <w lemma="his" pos="po" reg="his" xml:id="ncqx-001-a-1460">his</w>
          <w lemma="proclamation" pos="n1" reg="Proclamation" xml:id="ncqx-001-a-1470">Proclamation</w>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="ncqx-001-a-1480">to</w>
          <w lemma="publish" pos="vvi" reg="publish" xml:id="ncqx-001-a-1490">publish</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-1500">the</w>
          <w lemma="same" pos="d" reg="same" xml:id="ncqx-001-a-1510">same</w>
          <pc xml:id="ncqx-001-a-1520">,</pc>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="ncqx-001-a-1530">to</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-1540">the</w>
          <w lemma="end" pos="n1" reg="end" xml:id="ncqx-001-a-1550">ende</w>
          <pc xml:id="ncqx-001-a-1560">,</pc>
          <w lemma="that" pos="cs" reg="that" xml:id="ncqx-001-a-1570">that</w>
          <w lemma="all" pos="d" reg="all" xml:id="ncqx-001-a-1580">all</w>
          <w lemma="person" pos="n2" reg="people" xml:id="ncqx-001-a-1590">persons</w>
          <w lemma="who" pos="crq-ro" reg="whom" xml:id="ncqx-001-a-1600">whom</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-1610">the</w>
          <w lemma="same" pos="d" reg="same" xml:id="ncqx-001-a-1620">same</w>
          <w lemma="may" pos="vmb" reg="may" xml:id="ncqx-001-a-1630">may</w>
          <w lemma="concern" pos="vvi" reg="concern" xml:id="ncqx-001-a-1640">concerne</w>
          <pc xml:id="ncqx-001-a-1650">,</pc>
          <w lemma="may" pos="vmb" reg="may" xml:id="ncqx-001-a-1660">may</w>
          <w lemma="take" pos="vvi" reg="take" xml:id="ncqx-001-a-1670">take</w>
          <w lemma="notice" pos="n1" reg="notice" xml:id="ncqx-001-a-1680">notice</w>
          <w lemma="whither" pos="crq-cs" reg="whither" xml:id="ncqx-001-a-1690">whither</w>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="ncqx-001-a-1700">to</w>
          <w lemma="repair" pos="vvi" reg="repair" xml:id="ncqx-001-a-1710">repaire</w>
          <w lemma="upon" pos="acp-p" reg="upon" xml:id="ncqx-001-a-1720">vpon</w>
          <w lemma="all" pos="d" reg="all" xml:id="ncqx-001-a-1730">all</w>
          <w lemma="occasion" pos="n2" reg="occasions" xml:id="ncqx-001-a-1740">occasions</w>
          <w lemma="concern" pos="vvg" reg="concerning" xml:id="ncqx-001-a-1750">concerning</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-1760">the</w>
          <w lemma="bring" pos="n1_vg" reg="bringing" xml:id="ncqx-001-a-1770">bringing</w>
          <w lemma="in" pos="acp-p" reg="in" xml:id="ncqx-001-a-1780">in</w>
          <pc xml:id="ncqx-001-a-1790">,</pc>
          <w lemma="or" pos="cc" reg="or" xml:id="ncqx-001-a-1800">or</w>
          <w lemma="issue" pos="vvg" reg="issuing" xml:id="ncqx-001-a-1810">issuing</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="ncqx-001-a-1820">of</w>
          <w lemma="his" pos="po" reg="his" xml:id="ncqx-001-a-1830">his</w>
          <w lemma="majesty" pos="n1g" reg="Majesty's" xml:id="ncqx-001-a-1840">Maiesties</w>
          <w lemma="treasure" pos="n1" reg="Treasure" xml:id="ncqx-001-a-1850">Treasure</w>
          <w lemma="at" pos="acp-p" reg="at" xml:id="ncqx-001-a-1860">at</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-1870">the</w>
          <w lemma="receipt" pos="n1" reg="receipt" xml:id="ncqx-001-a-1880">receit</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="ncqx-001-a-1890">of</w>
          <w lemma="his" pos="po" reg="his" xml:id="ncqx-001-a-1900">his</w>
          <w lemma="exchequer" pos="n1" reg="Exchequer" xml:id="ncqx-001-a-1910">Exchequer</w>
          <pc unit="sentence" xml:id="ncqx-001-a-1920">.</pc>
          <w lemma="will" pos="vvg" reg="Willing" xml:id="ncqx-001-a-1930">Willing</w>
          <w lemma="and" pos="cc" reg="and" xml:id="ncqx-001-a-1940">and</w>
          <w lemma="require" pos="vvg" reg="requiring" xml:id="ncqx-001-a-1950">requiring</w>
          <w lemma="all" pos="d" reg="all" xml:id="ncqx-001-a-1960">all</w>
          <w lemma="sheriff" pos="n2" reg="Sheriffs" xml:id="ncqx-001-a-1970">Sheriffes</w>
          <pc xml:id="ncqx-001-a-1980">,</pc>
          <w lemma="bailiff" pos="n2" reg="Bailiffs" xml:id="ncqx-001-a-1990">Bailiffes</w>
          <pc xml:id="ncqx-001-a-2000">,</pc>
          <w lemma="collector" pos="n2" reg="Collectors" xml:id="ncqx-001-a-2010">Collectors</w>
          <pc xml:id="ncqx-001-a-2020">,</pc>
          <w lemma="and" pos="cc" reg="and" xml:id="ncqx-001-a-2030">and</w>
          <w lemma="all" pos="d" reg="all" xml:id="ncqx-001-a-2040">all</w>
          <w lemma="other" pos="d" reg="other" xml:id="ncqx-001-a-2050">other</w>
          <w lemma="officer" pos="n2" reg="officers" xml:id="ncqx-001-a-2060">officers</w>
          <pc xml:id="ncqx-001-a-2070">,</pc>
          <w lemma="accomptant" pos="n2" reg="Accomptants" xml:id="ncqx-001-a-2080">Accomptants</w>
          <pc xml:id="ncqx-001-a-2090">,</pc>
          <w lemma="and" pos="cc" reg="and" xml:id="ncqx-001-a-2100">and</w>
          <w lemma="person" pos="n2" reg="people" xml:id="ncqx-001-a-2110">persons</w>
          <w lemma="whatsoever" pos="crq-r" reg="whatsoever" xml:id="ncqx-001-a-2120">whatsoeuer</w>
          <pc xml:id="ncqx-001-a-2130">,</pc>
          <w lemma="who" pos="crq-r" reg="who" xml:id="ncqx-001-a-2140">who</w>
          <w lemma="be" pos="vvb" reg="are" xml:id="ncqx-001-a-2150">are</w>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="ncqx-001-a-2160">to</w>
          <w lemma="pay" pos="vvi" reg="pay" xml:id="ncqx-001-a-2170">pay</w>
          <w lemma="in" pos="acp-p" reg="in" xml:id="ncqx-001-a-2180">in</w>
          <w lemma="any" pos="d" reg="any" xml:id="ncqx-001-a-2190">any</w>
          <w lemma="money" pos="n2" reg="monies" xml:id="ncqx-001-a-2200">monies</w>
          <w lemma="into" pos="acp-p" reg="into" xml:id="ncqx-001-a-2210">into</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-2220">the</w>
          <w lemma="say" pos="j_vn" reg="said" xml:id="ncqx-001-a-2230">said</w>
          <w lemma="receipt" pos="n1" reg="receipt" xml:id="ncqx-001-a-2240">receit</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="ncqx-001-a-2250">of</w>
          <w lemma="his" pos="po" reg="his" xml:id="ncqx-001-a-2260">his</w>
          <w lemma="majesty" pos="n1g" reg="Majesty's" xml:id="ncqx-001-a-2270">Maiesties</w>
          <w lemma="exchequer" pos="n1" reg="Exchequer" xml:id="ncqx-001-a-2280">Exchequer</w>
          <pc xml:id="ncqx-001-a-2290">,</pc>
          <w lemma="or" pos="cc" reg="or" xml:id="ncqx-001-a-2300">or</w>
          <w lemma="otherwise" pos="av" reg="otherwise" xml:id="ncqx-001-a-2310">otherwise</w>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="ncqx-001-a-2320">to</w>
          <w lemma="attend" pos="vvi" reg="attend" xml:id="ncqx-001-a-2330">attend</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-2340">the</w>
          <w lemma="same" pos="d" reg="same" xml:id="ncqx-001-a-2350">same</w>
          <pc xml:id="ncqx-001-a-2360">,</pc>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="ncqx-001-a-2370">to</w>
          <w lemma="keep" pos="vvi" reg="keep" xml:id="ncqx-001-a-2380">keepe</w>
          <w lemma="their" pos="po" reg="their" xml:id="ncqx-001-a-2390">their</w>
          <w lemma="day" pos="n2" reg="days" xml:id="ncqx-001-a-2400">daies</w>
          <w lemma="and" pos="cc" reg="and" xml:id="ncqx-001-a-2410">and</w>
          <w lemma="time" pos="n2" reg="times" xml:id="ncqx-001-a-2420">times</w>
          <w lemma="at" pos="acp-p" reg="at" xml:id="ncqx-001-a-2430">at</w>
          <w lemma="Richmond" pos="n1-nn" reg="Richmond" xml:id="ncqx-001-a-2440">Richmond</w>
          <w lemma="aforesaid" pos="j" reg="aforesaid" xml:id="ncqx-001-a-2450">aforesaid</w>
          <pc xml:id="ncqx-001-a-2460">,</pc>
          <w lemma="and" pos="cc" reg="and" xml:id="ncqx-001-a-2470">and</w>
          <w lemma="there" pos="av" reg="there" xml:id="ncqx-001-a-2480">there</w>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="ncqx-001-a-2490">to</w>
          <w lemma="do" pos="vvi" reg="do" xml:id="ncqx-001-a-2500">doe</w>
          <pc xml:id="ncqx-001-a-2510">,</pc>
          <w lemma="pay" pos="vvb" reg="pay" xml:id="ncqx-001-a-2520">pay</w>
          <pc xml:id="ncqx-001-a-2530">,</pc>
          <w lemma="and" pos="cc" reg="and" xml:id="ncqx-001-a-2540">and</w>
          <w lemma="perform" pos="vvi" reg="perform" xml:id="ncqx-001-a-2550">performe</w>
          <w lemma="in" pos="acp-p" reg="in" xml:id="ncqx-001-a-2560">in</w>
          <w lemma="all" pos="d" reg="all" xml:id="ncqx-001-a-2570">all</w>
          <w lemma="thing" pos="n2" reg="things" xml:id="ncqx-001-a-2580">things</w>
          <w lemma="as" pos="acp-cs" reg="as" xml:id="ncqx-001-a-2590">as</w>
          <w lemma="they" pos="pns" reg="they" xml:id="ncqx-001-a-2600">they</w>
          <w lemma="shall" pos="vmd" reg="should" xml:id="ncqx-001-a-2610">should</w>
          <pc xml:id="ncqx-001-a-2620">,</pc>
          <w lemma="or" pos="cc" reg="or" xml:id="ncqx-001-a-2630">or</w>
          <w lemma="aught" pos="pi" reg="aught" xml:id="ncqx-001-a-2640">ought</w>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="ncqx-001-a-2650">to</w>
          <w lemma="have" pos="vvi" reg="have" xml:id="ncqx-001-a-2660">haue</w>
          <w lemma="do" pos="vvn" reg="done" xml:id="ncqx-001-a-2670">done</w>
          <w lemma="at" pos="acp-p" reg="at" xml:id="ncqx-001-a-2680">at</w>
          <w lemma="Westminster" pos="n1-nn" reg="Westminster" xml:id="ncqx-001-a-2690">Westminster</w>
          <pc xml:id="ncqx-001-a-2700">,</pc>
          <w lemma="if" pos="cs" reg="if" xml:id="ncqx-001-a-2710">if</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-2720">the</w>
          <w lemma="say" pos="j_vn" reg="said" xml:id="ncqx-001-a-2730">said</w>
          <w lemma="receipt" pos="n1" reg="receipt" xml:id="ncqx-001-a-2740">receit</w>
          <w lemma="of" pos="acp-p" reg="of" xml:id="ncqx-001-a-2750">of</w>
          <w lemma="exchequer" pos="n1" reg="Exchequer" xml:id="ncqx-001-a-2760">Exchequer</w>
          <w lemma="have" pos="vvd" reg="had" xml:id="ncqx-001-a-2770">had</w>
          <w lemma="continue" pos="vvn" reg="continued" xml:id="ncqx-001-a-2780">continued</w>
          <w lemma="there" pos="av" reg="there" xml:id="ncqx-001-a-2790">there</w>
          <pc unit="sentence" xml:id="ncqx-001-a-2800">.</pc>
          <w lemma="and" pos="cc" reg="And" xml:id="ncqx-001-a-2810">And</w>
          <w lemma="this" pos="d" reg="this" xml:id="ncqx-001-a-2820">this</w>
          <w lemma="to" pos="acp-cs" reg="to" xml:id="ncqx-001-a-2830">to</w>
          <w lemma="be" pos="vvi" reg="be" xml:id="ncqx-001-a-2840">bee</w>
          <w lemma="do" pos="vvn" reg="done" xml:id="ncqx-001-a-2850">done</w>
          <w lemma="and" pos="cc" reg="and" xml:id="ncqx-001-a-2860">and</w>
          <w lemma="observe" pos="vvn" reg="observed" xml:id="ncqx-001-a-2870">obserued</w>
          <w lemma="until" pos="acp-p" reg="until" xml:id="ncqx-001-a-2880">vntill</w>
          <w lemma="his" pos="po" reg="his" xml:id="ncqx-001-a-2890">his</w>
          <w lemma="majesty" pos="n1" reg="Majesty" xml:id="ncqx-001-a-2900">Maiestie</w>
          <w lemma="shall" pos="vmb" reg="shall" xml:id="ncqx-001-a-2910">shall</w>
          <w lemma="publish" pos="vvi" reg="publish" xml:id="ncqx-001-a-2920">publish</w>
          <w lemma="and" pos="cc" reg="and" xml:id="ncqx-001-a-2930">and</w>
          <w lemma="declare" pos="vvi" reg="declare" xml:id="ncqx-001-a-2940">declare</w>
          <w lemma="his" pos="po" reg="his" xml:id="ncqx-001-a-2950">his</w>
          <w lemma="further" pos="j-c" reg="further" xml:id="ncqx-001-a-2960">further</w>
          <w lemma="pleasure" pos="n1" reg="pleasure" xml:id="ncqx-001-a-2970">pleasure</w>
          <w lemma="to" pos="acp-p" reg="to" xml:id="ncqx-001-a-2980">to</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-2990">the</w>
          <w lemma="contrary" pos="n1_j" reg="contrary" xml:id="ncqx-001-a-3000">contrary</w>
          <pc unit="sentence" xml:id="ncqx-001-a-3010">.</pc>
        </p>
        <closer xml:id="ncqx-e50">
          <dateline xml:id="ncqx-e60">
            <w lemma="give" pos="vvn" reg="Given" xml:id="ncqx-001-a-3020">Given</w>
            <w lemma="at" pos="acp-p" reg="at" xml:id="ncqx-001-a-3030">at</w>
            <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-3040">the</w>
            <w lemma="court" pos="n1" reg="Court" xml:id="ncqx-001-a-3050">Court</w>
            <w lemma="at" pos="acp-p" reg="at" xml:id="ncqx-001-a-3060">at</w>
            <w lemma="Ricot" pos="n1-nn" reg="Ricot" xml:id="ncqx-001-a-3070">Ricot</w>
            <date xml:id="ncqx-e70">
              <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-3080">the</w>
              <w lemma="one" pos="crd" reg="one" xml:id="ncqx-001-a-3090">one</w>
              <w lemma="and" pos="cc" reg="and" xml:id="ncqx-001-a-3100">and</w>
              <w lemma="thirty" pos="ord" reg="thirtieth" xml:id="ncqx-001-a-3110">thirtieth</w>
              <w lemma="day" pos="n1" reg="day" xml:id="ncqx-001-a-3120">day</w>
              <w lemma="of" pos="acp-p" reg="of" xml:id="ncqx-001-a-3130">of</w>
              <w lemma="julie" pos="n1-nn" reg="julie" xml:id="ncqx-001-a-3140">Iulie</w>
              <w lemma="in" pos="acp-p" reg="in" xml:id="ncqx-001-a-3150">in</w>
              <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-3160">the</w>
              <w lemma="first" pos="ord" reg="first" xml:id="ncqx-001-a-3170">first</w>
              <w lemma="year" pos="n1" reg="year" xml:id="ncqx-001-a-3180">yeare</w>
              <w lemma="of" pos="acp-p" reg="of" xml:id="ncqx-001-a-3190">of</w>
              <w lemma="his" pos="po" reg="his" xml:id="ncqx-001-a-3200">his</w>
              <w lemma="majesty" pos="n1g" reg="Majesty's" xml:id="ncqx-001-a-3210">Maiesties</w>
              <w lemma="reign" pos="n1" reg="Reign" xml:id="ncqx-001-a-3220">Raigne</w>
              <w lemma="of" pos="acp-p" reg="of" xml:id="ncqx-001-a-3230">of</w>
              <w lemma="great" pos="j" reg="great" xml:id="ncqx-001-a-3240">great</w>
              <w lemma="Britain" pos="n1-nn" reg="Britain" xml:id="ncqx-001-a-3250">Brittaine</w>
              <pc xml:id="ncqx-001-a-3260">,</pc>
              <w lemma="France" pos="n1-nn" reg="France" xml:id="ncqx-001-a-3270">France</w>
              <pc xml:id="ncqx-001-a-3280">,</pc>
              <w lemma="and" pos="cc" reg="and" xml:id="ncqx-001-a-3290">and</w>
              <w lemma="Ireland" pos="n1-nn" reg="Ireland" xml:id="ncqx-001-a-3300">Ireland</w>
              <pc unit="sentence" xml:id="ncqx-001-a-3310">.</pc>
            </date>
          </dateline>
        </closer>
        <closer xml:id="ncqx-e80">
          <w lemma="God" pos="n1-nn" reg="God" xml:id="ncqx-001-a-3320">God</w>
          <w lemma="save" pos="acp-p" reg="save" xml:id="ncqx-001-a-3330">saue</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-3340">the</w>
          <w lemma="king." pos="n-ab" reg="King." xml:id="ncqx-001-a-3350">King.</w>
          <pc unit="sentence" xml:id="ncqx-001-a-3360"/>
        </closer>
      </div>
    </body>
    <back xml:id="ncqx-e90-b">
      <div type="colophon" xml:id="ncqx-e100">
        <p xml:id="ncqx-e110">
          <w lemma="print" pos="j_vn" reg="Printed" xml:id="ncqx-001-a-3370">Printed</w>
          <w lemma="at" pos="acp-p" reg="at" xml:id="ncqx-001-a-3380">at</w>
          <w lemma="Oxford" pos="n1-nn" reg="Oxford" xml:id="ncqx-001-a-3390">Oxford</w>
          <w lemma="by" pos="acp-p" reg="by" xml:id="ncqx-001-a-3400">by</w>
          <hi xml:id="ncqx-e120">
            <w lemma="i." pos="n-ab" reg="I" xml:id="ncqx-001-a-3410">I.</w>
            <w lemma="l." pos="n-ab" reg="L." xml:id="ncqx-001-a-3420">L.</w>
          </hi>
          <w lemma="and" pos="cc" reg="and" xml:id="ncqx-001-a-3430">and</w>
          <hi xml:id="ncqx-e130">
            <w lemma="W.T." pos="n1-nn" reg="W.T." xml:id="ncqx-001-a-3440">W.T.</w>
          </hi>
          <w lemma="for" pos="acp-p" reg="for" xml:id="ncqx-001-a-3450">for</w>
          <hi xml:id="ncqx-e140">
            <w lemma="bonham" pos="n1-nn" reg="Bonham" xml:id="ncqx-001-a-3460">Bonham</w>
            <w lemma="Norton" pos="n1-nn" reg="Norton" xml:id="ncqx-001-a-3470">Norton</w>
          </hi>
          <w lemma="and" pos="cc" reg="and" xml:id="ncqx-001-a-3480">and</w>
          <hi xml:id="ncqx-e150">
            <w lemma="John" pos="n1-nn" reg="john" xml:id="ncqx-001-a-3490">Iohn</w>
            <w lemma="bill" pos="n1" reg="Bill" xml:id="ncqx-001-a-3500">Bill</w>
            <pc xml:id="ncqx-001-a-3510">,</pc>
          </hi>
          <w lemma="printer" pos="n2" reg="Printers" xml:id="ncqx-001-a-3520">Printers</w>
          <w lemma="to" pos="acp-p" reg="to" xml:id="ncqx-001-a-3530">to</w>
          <w lemma="the" pos="d" reg="the" xml:id="ncqx-001-a-3540">the</w>
          <w lemma="king" pos="n2" reg="Kings" xml:id="ncqx-001-a-3550">Kings</w>
          <w lemma="most" pos="av-s_d" reg="most" xml:id="ncqx-001-a-3560">most</w>
          <w lemma="excellent" pos="j" reg="Excellent" xml:id="ncqx-001-a-3570">Excellent</w>
          <w lemma="majesty" pos="n1" reg="Majesty" xml:id="ncqx-001-a-3580">Maiestie</w>
          <pc unit="sentence" xml:id="ncqx-001-a-3590">.</pc>
          <w lemma="1625." pos="crd" reg="1625." xml:id="ncqx-001-a-3600">1625.</w>
          <pc unit="sentence" xml:id="ncqx-001-a-3610"/>
        </p>
      </div>
    </back>
  </text>
</TEI>

