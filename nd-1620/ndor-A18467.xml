<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Great king protect vs with thy gratious hand, Or else Armenius will o're spred this land ..</title>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>1628</date>
        </edition>
      </editionStmt>
      <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
      <publicationStmt>
        <publisher>Text Creation Partnership,</publisher>
        <pubPlace>Ann Arbor, MI ; Oxford (UK) :</pubPlace>
        <date>2008-09 (EEBO-TCP Phase 1).</date>
        <idno type="DLPS">A18467</idno>
        <idno type="STC">STC 5028</idno>
        <idno type="STC">ESTC S116815</idno>
        <idno type="EEBO-CITATION">99852031</idno>
        <idno type="PROQUEST">99852031</idno>
        <idno type="VID">17331</idno>
        <availability>
          <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
        </availability>
      </publicationStmt>
      <seriesStmt>
        <title>Early English books online.</title>
      </seriesStmt>
      <notesStmt>
        <note>(EEBO-TCP ; phase 1, no. A18467)</note>
        <note>Transcribed from: (Early English Books Online ; image set 17331)</note>
        <note>Images scanned from microfilm: (Early English books, 1475-1640 ; 1374:01)</note>
      </notesStmt>
      <sourceDesc>
        <biblFull>
          <titleStmt>
            <title>Great king protect vs with thy gratious hand, Or else Armenius will o're spred this land ..</title>
          </titleStmt>
          <extent>1 sheet ([1] p.) : ill.</extent>
          <publicationStmt>
            <publisher>Hendrick Laurentz excud. Amstelrodam,</publisher>
            <pubPlace>[Amsterdam] :</pubPlace>
            <date>1628.</date>
          </publicationStmt>
          <notesStmt>
            <note>A petition to King Charles I against the doctrines of Arminius.</note>
            <note>Engraving with letterpress verses printed on the margin below.</note>
            <note>The engraving, with imprint date altered to 1641 and with the verses engraved with added heading: "Englands Petition, ..", also appears as Wing E3011--STC.</note>
            <note>Title from first line of text.</note>
            <note>In verse.</note>
            <note>Reproduction of the original in the Bodleian Library.</note>
          </notesStmt>
        </biblFull>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <p>Created by converting TCP files to TEI P5 using tcp2tei.xsl, TEI @ Oxford.</p>
        <p>Re-processed by University of Nebraska-Lincoln and Northwestern, with changes to facilitate morpho-syntactic tagging. Gap elements of known extent have been transformed into placeholder characters or elements to simplify the filling in of gaps by user contributors.</p>
      </projectDesc>
      <editorialDecl>
        <p>EEBO-TCP is a partnership between the Universities of Michigan and Oxford and the publisher ProQuest to create accurately transcribed and encoded texts based on the image sets published by ProQuest via their Early English Books Online (EEBO) database (http://eebo.chadwyck.com). The general aim of EEBO-TCP is to encode one copy (usually the first edition) of every monographic English-language title published between 1473 and 1700 available in EEBO.</p>
        <p>EEBO-TCP aimed to produce large quantities of textual data within the usual project restraints of time and funding, and therefore chose to create diplomatic transcriptions (as opposed to critical editions) with light-touch, mainly structural encoding based on the Text Encoding Initiative (http://www.tei-c.org).</p>
        <p>The EEBO-TCP project was divided into two phases. The 25,363 texts created during Phase 1 of the project have been released into the public domain as of 1 January 2015. Anyone can now take and use these texts for their own purposes, but we respectfully request that due credit and attribution is given to their original source.</p>
        <p>Users should be aware of the process of creating the TCP texts, and therefore of any assumptions that can be made about the data.</p>
        <p>Text selection was based on the New Cambridge Bibliography of English Literature (NCBEL). If an author (or for an anonymous work, the title) appears in NCBEL, then their works are eligible for inclusion. Selection was intended to range over a wide variety of subject areas, to reflect the true nature of the print record of the period. In general, first editions of a works in English were prioritized, although there are a number of works in other languages, notably Latin and Welsh, included and sometimes a second or later edition of a work was chosen if there was a compelling reason to do so.</p>
        <p>Image sets were sent to external keying companies for transcription and basic encoding. Quality assurance was then carried out by editorial teams in Oxford and Michigan. 5% (or 5 pages, whichever is the greater) of each text was proofread for accuracy and those which did not meet QA standards were returned to the keyers to be redone. After proofreading, the encoding was enhanced and/or corrected and characters marked as illegible were corrected where possible up to a limit of 100 instances per text. Any remaining illegibles were encoded as &lt;gap&gt;s. Understanding these processes should make clear that, while the overall quality of TCP data is very good, some errors will remain and some readable characters will be marked as illegible. Users should bear in mind that in all likelihood such instances will never have been looked at by a TCP editor.</p>
        <p>The texts were encoded and linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
        <p>Copies of the texts have been issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
        <p>
          Keying and markup guidelines are available at the
          <ref target="http://www.textcreationpartnership.org/docs/.">Text Creation Partnership web site</ref>
          .
        </p>
      </editorialDecl>
      <listPrefixDef>
        <prefixDef ident="tcp" matchPattern="([0-9\-]+):([0-9IVX]+)" replacementPattern="http://eebo.chadwyck.com/downloadtiff?vid=$1&amp;page=$2"/>
        <prefixDef ident="char" matchPattern="(.+)" replacementPattern="https://raw.githubusercontent.com/textcreationpartnership/Texts/master/tcpchars.xml#$1"/>
      </listPrefixDef>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="eng">eng</language>
      </langUsage>
      <textClass>
        <keywords scheme="http://authorities.loc.gov/">
          <term>Arminianism -- England -- Early works to 1800.</term>
        </keywords>
      </textClass>
    </profileDesc>
    <revisionDesc>
      <change><date>2007-08</date><label>TCP</label>
        Assigned for keying and markup
      </change>
      <change><date>2007-08</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
      <change><date>2007-09</date><label>Elspeth Healey</label>
        Sampled and proofread
      </change>
      <change><date>2007-09</date><label>Elspeth Healey</label>
        Text and markup reviewed and edited
      </change>
      <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
    </revisionDesc>
  </teiHeader>
  <text xml:id="ndor-t">
    <body xml:id="ndor-e0">
      <div type="poem" xml:id="ndor-e10">
        <pb facs="tcp:17331:1" xml:id="ndor-001-a"/>
        <p xml:id="ndor-e20">
          <figure xml:id="ndor-e30">
            <p xml:id="ndor-e40">
              <w lemma="biblia" pos="fw-la" reg="BIBLIA" xml:id="ndor-001-a-0010">BIBLIA</w>
            </p>
            <p xml:id="ndor-e50">
              <w lemma="veritas" pos="fw-la" reg="Veritas" xml:id="ndor-001-a-0020">Veritas</w>
              <pc unit="sentence" xml:id="ndor-001-a-0030">.</pc>
            </p>
            <p xml:id="ndor-e60">
              <w lemma="Heresia" pos="n1-nn" reg="Heresia" xml:id="ndor-001-a-0040">Heresia</w>
              <pc unit="sentence" xml:id="ndor-001-a-0050">.</pc>
            </p>
            <byline xml:id="ndor-e70">
              <w lemma="hendrick" pos="n1-nn" reg="Hendrick" xml:id="ndor-001-a-0060">Hendrick</w>
              <w lemma="Laurentz" pos="n1-nn" reg="Laurentz" xml:id="ndor-001-a-0070">Laurentz</w>
              <w lemma="excud" pos="n1" reg="excud" xml:id="ndor-001-a-0080">excud</w>
              <pc unit="sentence" xml:id="ndor-001-a-0090">.</pc>
              <w lemma="Amstelrodam" pos="n1-nn" reg="Amstelrodam" xml:id="ndor-001-a-0100">Amstelrodam</w>
              <pc unit="sentence" xml:id="ndor-001-a-0110">.</pc>
              <w lemma="1628." pos="crd" reg="1628." xml:id="ndor-001-a-0120">1628.</w>
              <pc unit="sentence" xml:id="ndor-001-a-0130"/>
            </byline>
          </figure>
        </p>
        <lg xml:id="ndor-e80">
          <l xml:id="ndor-e90">
            <w lemma="great" pos="j" reg="Great" xml:id="ndor-001-a-0140">GReat</w>
            <w lemma="king" pos="n1" reg="King" xml:id="ndor-001-a-0150">King</w>
            <w lemma="protect" pos="vvb" reg="protect" xml:id="ndor-001-a-0160">protect</w>
            <w lemma="we" pos="pno" reg="us" xml:id="ndor-001-a-0170">vs</w>
            <w lemma="with" pos="acp-p" reg="with" xml:id="ndor-001-a-0180">with</w>
            <w lemma="thy" pos="po" reg="thy" xml:id="ndor-001-a-0190">thy</w>
            <w lemma="gracious" pos="j" reg="gracious" xml:id="ndor-001-a-0200">gratious</w>
            <w lemma="hand" pos="n1" reg="hand" xml:id="ndor-001-a-0210">hand</w>
            <pc xml:id="ndor-001-a-0220">,</pc>
          </l>
          <l xml:id="ndor-e100">
            <w lemma="or" pos="cc" reg="Or" xml:id="ndor-001-a-0230">Or</w>
            <w lemma="else" pos="av" reg="else" xml:id="ndor-001-a-0240">else</w>
            <w lemma="armenius" pos="n1-nn" reg="Armenius" xml:id="ndor-001-a-0250" rendition="#hi">Armenius</w>
            
            <w lemma="will" pos="vmb" reg="will" xml:id="ndor-001-a-0260">will</w>
            <w lemma="over" pos="acp-p" reg="o'er" xml:id="ndor-001-a-0270">o're</w>
            <w lemma="spread" pos="vvi" reg="spread" xml:id="ndor-001-a-0280">spred</w>
            <w lemma="this" pos="d" reg="this" xml:id="ndor-001-a-0290">this</w>
            <w lemma="land" pos="n1" reg="Land" xml:id="ndor-001-a-0300">Land</w>
            <pc xml:id="ndor-001-a-0310">:</pc>
          </l>
          <l xml:id="ndor-e120">
            <w lemma="for" pos="acp-cs" reg="For" xml:id="ndor-001-a-0320">For</w>
            <w lemma="if" pos="cs" reg="if" xml:id="ndor-001-a-0330">if</w>
            <w lemma="in" pos="acp-p" reg="in" xml:id="ndor-001-a-0340">in</w>
            <w lemma="England" pos="n1-nn" reg="England" xml:id="ndor-001-a-0350" rendition="#hi">England</w>
            
            <w lemma="the" pos="d" reg="th'" xml:id="ndor-001-a-0360" type="contract1">th'</w><w xml:id="ndor-001-a-0360c" type="contract2" lemma="enemy" pos="n1" reg="enemy" join="left">enemy</w>
            <w lemma="do" pos="vvz" reg="doth" xml:id="ndor-001-a-0370">doth</w>
            <w lemma="appear" pos="vvi" reg="appear" xml:id="ndor-001-a-0380">appeare</w>
            <pc xml:id="ndor-001-a-0390">,</pc>
          </l>
          <l xml:id="ndor-e140">
            <w lemma="this" pos="d" reg="This" xml:id="ndor-001-a-0400">This</w>
            <w lemma="be" pos="vvz" reg="is" xml:id="ndor-001-a-0410">is</w>
            <w lemma="the" pos="d" reg="the" xml:id="ndor-001-a-0420">the</w>
            <w lemma="shape" pos="n1" reg="shape" xml:id="ndor-001-a-0430">shape</w>
            <w lemma="of" pos="acp-p" reg="of" xml:id="ndor-001-a-0440">of</w>
            <w lemma="he" pos="pno" reg="him" xml:id="ndor-001-a-0450">him</w>
            <w lemma="we" pos="pns" reg="we" xml:id="ndor-001-a-0460">we</w>
            <w lemma="need" pos="vvb" reg="need" xml:id="ndor-001-a-0470">need</w>
            <w lemma="to" pos="acp-cs" reg="to" xml:id="ndor-001-a-0480">to</w>
            <w lemma="fear" pos="vvi" reg="fear" xml:id="ndor-001-a-0490">feare</w>
            <pc unit="sentence" xml:id="ndor-001-a-0500">.</pc>
          </l>
          <l xml:id="ndor-e150">
            <w lemma="he" pos="pns" reg="He" xml:id="ndor-001-a-0510">He</w>
            <w lemma="raise" pos="vvz" reg="raiseth" xml:id="ndor-001-a-0520">raiseth</w>
            <w lemma="faction" pos="n2" reg="Factions" xml:id="ndor-001-a-0530">Factions</w>
            <pc xml:id="ndor-001-a-0540">,</pc>
            <w lemma="and" pos="cc" reg="and" xml:id="ndor-001-a-0550">and</w>
            <w lemma="that" pos="cs" reg="that" xml:id="ndor-001-a-0560">that</w>
            <w lemma="bring" pos="vvz" reg="brings" xml:id="ndor-001-a-0570">brings</w>
            <w lemma="in" pos="acp-p" reg="in" xml:id="ndor-001-a-0580">in</w>
            <w lemma="jar" pos="n2" reg="jars" xml:id="ndor-001-a-0590">iarres</w>
            <pc xml:id="ndor-001-a-0600">,</pc>
          </l>
          <l xml:id="ndor-e160">
            <w lemma="which" pos="crq-r" reg="Which" xml:id="ndor-001-a-0610">Which</w>
            <w lemma="broach" pos="vvz" reg="broacheth" xml:id="ndor-001-a-0620">broacheth</w>
            <w lemma="error" pos="n2" reg="Errors" xml:id="ndor-001-a-0630">Errors</w>
            <pc xml:id="ndor-001-a-0640">,</pc>
            <w lemma="and" pos="cc" reg="and" xml:id="ndor-001-a-0650">and</w>
            <w lemma="uphold" pos="vvz" reg="upholds" xml:id="ndor-001-a-0660">vpholds</w>
            <w lemma="the" pos="d" reg="the" xml:id="ndor-001-a-0670">the</w>
            <w lemma="war" pos="n2" reg="wars" xml:id="ndor-001-a-0680">wars</w>
            <pc xml:id="ndor-001-a-0690">:</pc>
          </l>
          <l xml:id="ndor-e170">
            <w lemma="the" pos="d" reg="The" xml:id="ndor-001-a-0700">The</w>
            <w lemma="Netherlands" pos="n1-nn" reg="Netherlands" xml:id="ndor-001-a-0710" rendition="#hi">Netherlands</w>
            
            <w lemma="ruin" pos="n1" reg="ruin" xml:id="ndor-001-a-0720">ruine</w>
            <pc xml:id="ndor-001-a-0730">,</pc>
            <w lemma="he" pos="pns" reg="he" xml:id="ndor-001-a-0740">he</w>
            <w lemma="seek" pos="vvd" reg="sought" xml:id="ndor-001-a-0750">sought</w>
            <w lemma="to" pos="acp-cs" reg="to" xml:id="ndor-001-a-0760">to</w>
            <w lemma="bring" pos="vvi" reg="bring" xml:id="ndor-001-a-0770">bring</w>
            <pc xml:id="ndor-001-a-0780">,</pc>
          </l>
          <l xml:id="ndor-e190">
            <w lemma="in" pos="acp-p" reg="In" xml:id="ndor-001-a-0790">In</w>
            <w lemma="England" pos="n1-nn" reg="England" xml:id="ndor-001-a-0800" rendition="#hi">England</w>
            
            <w lemma="now" pos="av" reg="now" xml:id="ndor-001-a-0810">now</w>
            <w lemma="he" pos="pns" reg="he" xml:id="ndor-001-a-0820">he</w>
            <w lemma="do" pos="vvz" reg="doth" xml:id="ndor-001-a-0830">doth</w>
            <w lemma="the" pos="d" reg="the" xml:id="ndor-001-a-0840">the</w>
            <w lemma="self" pos="n1" reg="self" xml:id="ndor-001-a-0850">selfe</w>
            <w lemma="same" pos="d" reg="same" xml:id="ndor-001-a-0860">same</w>
            <w lemma="thing" pos="n1" reg="thing" xml:id="ndor-001-a-0870">thing</w>
            <pc unit="sentence" xml:id="ndor-001-a-0880">.</pc>
          </l>
          <l xml:id="ndor-e210">
            <w lemma="to" pos="acp-cs" reg="To" xml:id="ndor-001-a-0890">To</w>
            <w lemma="rail" pos="vvi" reg="rail" xml:id="ndor-001-a-0900">rayle</w>
            <pc xml:id="ndor-001-a-0910">,</pc>
            <w lemma="to" pos="acp-cs" reg="to" xml:id="ndor-001-a-0920">to</w>
            <w lemma="write" pos="vvi" reg="writ" xml:id="ndor-001-a-0930">write</w>
            <pc xml:id="ndor-001-a-0940">,</pc>
            <w lemma="to" pos="acp-cs" reg="to" xml:id="ndor-001-a-0950">to</w>
            <w lemma="publish" pos="vvi" reg="publish" xml:id="ndor-001-a-0960">publish</w>
            <w lemma="bitter" pos="j" reg="bitter" xml:id="ndor-001-a-0970">bitter</w>
            <w lemma="gall" pos="n1" reg="gall" xml:id="ndor-001-a-0980">gall</w>
            <pc xml:id="ndor-001-a-0990">,</pc>
          </l>
          <l xml:id="ndor-e220">
            <w lemma="to" pos="acp-cs" reg="To" xml:id="ndor-001-a-1000">To</w>
            <w lemma="change" pos="vvi" reg="change" xml:id="ndor-001-a-1010">change</w>
            <w lemma="religion" pos="n1" reg="Religion" xml:id="ndor-001-a-1020">Religi●n</w>
            <pc xml:id="ndor-001-a-1030">,</pc>
            <w lemma="and" pos="cc" reg="and" xml:id="ndor-001-a-1040">and</w>
            <w lemma="subvert" pos="vvi" reg="subvert" xml:id="ndor-001-a-1050">subuert</w>
            <w lemma="we" pos="pno" reg="us" xml:id="ndor-001-a-1060">vs</w>
            <w lemma="all" pos="d" reg="all" xml:id="ndor-001-a-1070">all</w>
            <pc unit="sentence" xml:id="ndor-001-a-1080">.</pc>
          </l>
          <l xml:id="ndor-e230">
            <w lemma="his" pos="po" reg="His" xml:id="ndor-001-a-1090">His</w>
            <w lemma="squint-eyed" pos="j" reg="Squint-eyed" xml:id="ndor-001-a-1100">Squint-ey'd</w>
            <w lemma="look" pos="n2" reg="looks" xml:id="ndor-001-a-1110">lookes</w>
            <w xml:id="ndor-001-a-1120" lemma="&amp;" reg="&amp;" pos="cc">&amp;</w>
            <w lemma="lansa-wolsa" pos="j" reg="Lansa-wolsa" xml:id="ndor-001-a-1130" rendition="#hi">Lansa-Wolsa</w>
            
            <w lemma="gown" pos="n1" reg="gown" xml:id="ndor-001-a-1140">gowne</w>
            <pc xml:id="ndor-001-a-1150">,</pc>
          </l>
          <l xml:id="ndor-e250">
            <w lemma="show" pos="vvz" reg="Shows" xml:id="ndor-001-a-1160">Shewes</w>
            <w lemma="how" pos="crq-cs" reg="how" xml:id="ndor-001-a-1170">how</w>
            <w lemma="religion" pos="n1" reg="Religion" xml:id="ndor-001-a-1180">Religion</w>
            <w lemma="h●" pos="n1" reg="h●" xml:id="ndor-001-a-1190">h●</w>
            <w lemma="will" pos="vmb" reg="will" xml:id="ndor-001-a-1200">wil</w>
            <w lemma="soon" pos="av" reg="soon" xml:id="ndor-001-a-1210">soone</w>
            <w lemma="throw" pos="vvi" reg="throw" xml:id="ndor-001-a-1220">throw</w>
            <w lemma="down" pos="acp-av" reg="down" xml:id="ndor-001-a-1230">down</w>
            <pc unit="sentence" xml:id="ndor-001-a-1240">.</pc>
          </l>
          <l xml:id="ndor-e260">
            <w lemma="his" pos="po" reg="His" xml:id="ndor-001-a-1250">His</w>
            <w lemma="grind" pos="vvg" reg="grinding" xml:id="ndor-001-a-1260">grynding</w>
            <w lemma="pate" pos="n1" reg="pate" xml:id="ndor-001-a-1270">pate</w>
            <w lemma="with" pos="acp-p" reg="with" xml:id="ndor-001-a-1280">with</w>
            <w lemma="weathercock" pos="n2" reg="weathercocks" xml:id="ndor-001-a-1290">weather-Cocks</w>
            <w lemma="turn" pos="vvn" reg="turned" xml:id="ndor-001-a-1300">turn'd</w>
            <w lemma="brain" pos="n1" reg="brain" xml:id="ndor-001-a-1310">braine</w>
            <pc xml:id="ndor-001-a-1320">,</pc>
          </l>
          <l xml:id="ndor-e270">
            <w lemma="seek" pos="vvz" reg="Seeks" xml:id="ndor-001-a-1330">Seeketh</w>
            <w lemma="the" pos="d" reg="the" xml:id="ndor-001-a-1340">the</w>
            <w lemma="church" pos="n1g" reg="Church's" xml:id="ndor-001-a-1350" rendition="#hi">Churches</w>
            
            <w lemma="tenet" pos="n2" reg="tenets" xml:id="ndor-001-a-1360">tenets</w>
            <w lemma="for" pos="acp-p" reg="for" xml:id="ndor-001-a-1370">for</w>
            <w lemma="to" pos="acp-cs" reg="to" xml:id="ndor-001-a-1380">to</w>
            <w lemma="stain" pos="vvi" reg="stain" xml:id="ndor-001-a-1390">staine</w>
            <pc xml:id="ndor-001-a-1400">:</pc>
          </l>
          <l xml:id="ndor-e290">
            <w lemma="the" pos="d" reg="The" xml:id="ndor-001-a-1410">The</w>
            <w lemma="crystal" pos="n1" reg="Crystal" xml:id="ndor-001-a-1420">Christal</w>
            <w lemma="stream" pos="n2" reg="streams" xml:id="ndor-001-a-1430">streams</w>
            <w lemma="of" pos="acp-p" reg="of" xml:id="ndor-001-a-1440">of</w>
            <w lemma="truth" pos="n1" reg="truth" xml:id="ndor-001-a-1450">truth</w>
            <w lemma="he" pos="pns" reg="he" xml:id="ndor-001-a-1460">he</w>
            <w lemma="shun" pos="vvz" reg="shuns" xml:id="ndor-001-a-1470">shuns</w>
            <w lemma="most" pos="av-s_d" reg="most" xml:id="ndor-001-a-1480">most</w>
            <w lemma="pure" pos="j" reg="pure" xml:id="ndor-001-a-1490">pure</w>
            <pc xml:id="ndor-001-a-1500">,</pc>
          </l>
          <l xml:id="ndor-e300">
            <w lemma="the" pos="d" reg="The" xml:id="ndor-001-a-1510">The</w>
            <w lemma="trial" pos="n1" reg="trial" xml:id="ndor-001-a-1520">tryall</w>
            <w lemma="of" pos="acp-p" reg="of" xml:id="ndor-001-a-1530">of</w>
            <w lemma="God" pos="n1g-nn" reg="God's" xml:id="ndor-001-a-1540" rendition="#hi">Gods</w>
            
            <w lemma="word" pos="n1" reg="word" xml:id="ndor-001-a-1550">word</w>
            <w lemma="he" pos="pns" reg="he" xml:id="ndor-001-a-1560" type="contract1">he</w><w xml:id="ndor-001-a-1560c" type="contract2" lemma="will" pos="vmb" reg="'ll" join="left">'le</w>
            <w lemma="not" pos="xx" reg="not" xml:id="ndor-001-a-1570">not</w>
            <w lemma="endure" pos="vvi" reg="endure" xml:id="ndor-001-a-1580">endure</w>
            <pc xml:id="ndor-001-a-1590">:</pc>
          </l>
          <l xml:id="ndor-e320">
            <w lemma="but" pos="acp-cc" reg="But" xml:id="ndor-001-a-1600">But</w>
            <w lemma="unto" pos="acp-p" reg="unto" xml:id="ndor-001-a-1610">vnto</w>
            <w lemma="error" pos="n1" reg="Error" xml:id="ndor-001-a-1620" rendition="#hi">Error</w>
            
            <w lemma="cast" pos="vvi" reg="cast" xml:id="ndor-001-a-1630">cast</w>
            <w lemma="his" pos="po" reg="his" xml:id="ndor-001-a-1640">his</w>
            <w lemma="blink" pos="j_vg" reg="blinking" xml:id="ndor-001-a-1650">blinking</w>
            <w lemma="eye" pos="n1" reg="eye" xml:id="ndor-001-a-1660">eye</w>
            <pc xml:id="ndor-001-a-1670">,</pc>
          </l>
          <l xml:id="ndor-e340">
            <w lemma="presume" pos="vvg" reg="Presuming" xml:id="ndor-001-a-1680">Presuming</w>
            <w lemma="truth" pos="n1" reg="Truth" xml:id="ndor-001-a-1690" rendition="#hi">Truth</w>
            
            <w lemma="do" pos="vvz" reg="doth" xml:id="ndor-001-a-1700">doth</w>
            <w lemma="not" pos="xx" reg="not" xml:id="ndor-001-a-1710">not</w>
            <w lemma="the" pos="d" reg="the" xml:id="ndor-001-a-1720">the</w>
            <w lemma="same" pos="d" reg="same" xml:id="ndor-001-a-1730">same</w>
            <w lemma="espy" pos="vvi" reg="espy" xml:id="ndor-001-a-1740">espie</w>
            <pc unit="sentence" xml:id="ndor-001-a-1750">.</pc>
          </l>
          <l xml:id="ndor-e360">
            <w lemma="heresy" pos="n1" reg="Heresy" xml:id="ndor-001-a-1760" rendition="#hi">Heresie</w>
            
            <w lemma="upon" pos="acp-p" reg="upon" xml:id="ndor-001-a-1770">vpon</w>
            <w lemma="a" pos="d" reg="a" xml:id="ndor-001-a-1780">a</w>
            <w lemma="stately" pos="j" reg="stately" xml:id="ndor-001-a-1790">stately</w>
            <w lemma="beast" pos="n1" reg="Beast" xml:id="ndor-001-a-1800" rendition="#hi">Beast</w>
            
            <w lemma="do" pos="vvz" reg="doth" xml:id="ndor-001-a-1810">doth</w>
            <w lemma="stand" pos="vvi" reg="stand" xml:id="ndor-001-a-1820">stand</w>
            <pc xml:id="ndor-001-a-1830">,</pc>
          </l>
          <l xml:id="ndor-e390">
            <w lemma="armenius" pos="n1-nn" reg="Armenius" xml:id="ndor-001-a-1840" rendition="#hi">Armenius</w>
            
            <w lemma="bid" pos="vvz" reg="bids" xml:id="ndor-001-a-1850">bids</w>
            <w lemma="he" pos="pno" reg="him" xml:id="ndor-001-a-1860">him</w>
            <w lemma="welcome" pos="j" reg="welcome" xml:id="ndor-001-a-1870">welcome</w>
            <pc xml:id="ndor-001-a-1880">,</pc>
            <w lemma="hold" pos="vvz" reg="holds" xml:id="ndor-001-a-1890">holds</w>
            <w lemma="his" pos="po" reg="his" xml:id="ndor-001-a-1900">his</w>
            <w lemma="hand" pos="n1" reg="hand" xml:id="ndor-001-a-1910">hand</w>
            <pc unit="sentence" xml:id="ndor-001-a-1920">.</pc>
          </l>
          <l xml:id="ndor-e410">
            <w lemma="truth" pos="n1" reg="Truth" xml:id="ndor-001-a-1930" rendition="#hi">Truth</w>
            
            <w lemma="by" pos="acp-p" reg="by" xml:id="ndor-001-a-1940">by</w>
            <w lemma="her" pos="po" reg="her" xml:id="ndor-001-a-1950">her</w>
            <w lemma="brightness" pos="n1" reg="brightness" xml:id="ndor-001-a-1960">brightnesse</w>
            <pc xml:id="ndor-001-a-1970">,</pc>
            <w lemma="and" pos="cc" reg="and" xml:id="ndor-001-a-1980">and</w>
            <w lemma="her" pos="po" reg="her" xml:id="ndor-001-a-1990">her</w>
            <w lemma="sincere" pos="j" reg="sincere" xml:id="ndor-001-a-2000">sincere</w>
            <w lemma="heart" pos="n1" reg="heart" xml:id="ndor-001-a-2010">heart</w>
            <pc xml:id="ndor-001-a-2020">,</pc>
          </l>
          <l xml:id="ndor-e430">
            <w lemma="show" pos="vvz" reg="Shows" xml:id="ndor-001-a-2030">Shewes</w>
            <w lemma="that" pos="d" reg="that" xml:id="ndor-001-a-2040">that</w>
            <w lemma="with" pos="acp-p" reg="with" xml:id="ndor-001-a-2050">with</w>
            <w lemma="heresy" pos="n1" reg="Heresy" xml:id="ndor-001-a-2060" rendition="#hi">Heresie</w>
            
            <w lemma="she" pos="pns" reg="she" xml:id="ndor-001-a-2070">shee</w>
            <w lemma="take" pos="vvz" reg="takes" xml:id="ndor-001-a-2080">takes</w>
            <w lemma="not" pos="xx" reg="not" xml:id="ndor-001-a-2090">no</w>
            <w lemma="par" pos="fw-fr" reg="par" xml:id="ndor-001-a-2100">par</w>
            <pc unit="sentence" xml:id="ndor-001-a-2110">.</pc>
          </l>
          <l xml:id="ndor-e450">
            <w lemma="tread" pos="vvz" reg="Treads" xml:id="ndor-001-a-2120">Treades</w>
            <w lemma="on" pos="acp-p" reg="on" xml:id="ndor-001-a-2130">on</w>
            <w lemma="their" pos="po" reg="their" xml:id="ndor-001-a-2140">their</w>
            <hi xml:id="ndor-e460">
              <w lemma="mounteb" pos="n1" reg="Mounteb" xml:id="ndor-001-a-2150">Mounteb</w>
              <w lemma="ink" pos="n1" reg="ink" xml:id="ndor-001-a-2160">inke</w>
              <w xml:id="ndor-001-a-2170" lemma="&amp;" reg="&amp;" pos="cc">&amp;</w>
              <w lemma="cozen" pos="j_vg" reg="Cozening" xml:id="ndor-001-a-2180">Cozning</w>
            </hi>
            <w lemma="trick" pos="n2" reg="tricks" xml:id="ndor-001-a-2190">tricks</w>
            <pc xml:id="ndor-001-a-2200">,</pc>
          </l>
          <l xml:id="ndor-e470">
            <w lemma="blow" pos="vvn" reg="Blown" xml:id="ndor-001-a-2210">Blowne</w>
            <w lemma="in" pos="acp-p" reg="in" xml:id="ndor-001-a-2220">in</w>
            <w lemma="his" pos="po" reg="his" xml:id="ndor-001-a-2230">his</w>
            <w lemma="ear" pos="n2" reg="ears" xml:id="ndor-001-a-2240">eares</w>
            <pc xml:id="ndor-001-a-2250">,</pc>
            <w lemma="by" pos="acp-p" reg="by" xml:id="ndor-001-a-2260">by</w>
            <w lemma="Pe●agius" pos="n1-nn" reg="Pe●agius" xml:id="ndor-001-a-2270" rendition="#hi">Pe●agius</w>
            
            <w lemma="and" pos="cc" reg="and" xml:id="ndor-001-a-2280">and</w>
            <w lemma="jesuite" pos="n2-nn" reg="jesuites" xml:id="ndor-001-a-2290" rendition="#hi">Iesuites</w><pc unit="sentence" xml:id="ndor-001-a-2300">.</pc>
            
              
          </l>
          <l xml:id="ndor-e500">
            <w lemma="which" pos="crq-r" reg="Which" xml:id="ndor-001-a-2310">Which</w>
            <w lemma="make" pos="vvz" reg="makes" xml:id="ndor-001-a-2320">makes</w>
            <w lemma="his" pos="po" reg="his" xml:id="ndor-001-a-2330">his</w>
            <w lemma="windmill" pos="n1" reg="Windmill" xml:id="ndor-001-a-2340" rendition="#hi">Wind-mil</w>
            
            <w lemma="for" pos="acp-p" reg="for" xml:id="ndor-001-a-2350">for</w>
            <w lemma="promotion" pos="n2" reg="promotions" xml:id="ndor-001-a-2360">promotions</w>
            <w lemma="grace" pos="n1" reg="grace" xml:id="ndor-001-a-2370">grace</w>
          </l>
          <l xml:id="ndor-e520">
            <w lemma="publish" pos="vvb" reg="Publish" xml:id="ndor-001-a-2380">Publish</w>
            <w lemma="his" pos="po" reg="his" xml:id="ndor-001-a-2390">his</w>
            <w lemma="book" pos="n2" reg="Books" xml:id="ndor-001-a-2400">Bookes</w>
            <w lemma="abroad" pos="av" reg="abroad" xml:id="ndor-001-a-2410">abroad</w>
            <w lemma="in" pos="acp-p" reg="in" xml:id="ndor-001-a-2420">in</w>
            <w lemma="every" pos="d" reg="every" xml:id="ndor-001-a-2430">euery</w>
            <w lemma="place" pos="n1" reg="place" xml:id="ndor-001-a-2440">place</w>
            <pc xml:id="ndor-001-a-2450">:</pc>
          </l>
          <l xml:id="ndor-e530">
            <w lemma="and" pos="cc" reg="And" xml:id="ndor-001-a-2460">And</w>
            <w lemma="beg" pos="vvz" reg="begs" xml:id="ndor-001-a-2470">begs</w>
            <w lemma="protection" pos="n1" reg="protection" xml:id="ndor-001-a-2480">protection</w>
            <w lemma="for" pos="acp-p" reg="for" xml:id="ndor-001-a-2490">for</w>
            <w lemma="his" pos="po" reg="his" xml:id="ndor-001-a-2500">his</w>
            <w lemma="work" pos="n2" reg="works" xml:id="ndor-001-a-2510">workes</w>
            <w lemma="of" pos="acp-p" reg="of" xml:id="ndor-001-a-2520">of</w>
            <w lemma="wonder" pos="n1" reg="wonder" xml:id="ndor-001-a-2530">wonder</w>
            <pc xml:id="ndor-001-a-2540">,</pc>
          </l>
          <l xml:id="ndor-e540">
            <w lemma="which" pos="crq-r" reg="Which" xml:id="ndor-001-a-2550">Which</w>
            <w lemma="against" pos="acp-p" reg="against" xml:id="ndor-001-a-2560">against</w>
            <w lemma="truth" pos="n1" reg="Truth" xml:id="ndor-001-a-2570" rendition="#hi">Truth</w>
            
            <w lemma="he" pos="pns" reg="he" xml:id="ndor-001-a-2580">he</w>
            <w lemma="bellows" pos="n2" reg="bellowss" xml:id="ndor-001-a-2590">bellowes</w>
            <w lemma="forth" pos="av" reg="forth" xml:id="ndor-001-a-2600">forth</w>
            <w lemma="like" pos="acp-p" reg="like" xml:id="ndor-001-a-2610">like</w>
            <w lemma="thunder" pos="n1" reg="thunder" xml:id="ndor-001-a-2620">thunder</w>
            <pc unit="sentence" xml:id="ndor-001-a-2630">.</pc>
          </l>
          <l xml:id="ndor-e560">
            <w lemma="thus" pos="av" reg="Thus" xml:id="ndor-001-a-2640">Thus</w>
            <w lemma="do" pos="vvz" reg="doth" xml:id="ndor-001-a-2650">doth</w>
            <w lemma="armenius" pos="n1-nn" reg="Armenius" xml:id="ndor-001-a-2660" rendition="#hi">Armenius</w>
            
            <w lemma="to" pos="acp-p" reg="to" xml:id="ndor-001-a-2670">to</w>
            <w lemma="preferment" pos="n1" reg="preferment" xml:id="ndor-001-a-2680">preferment</w>
            <w lemma="rise" pos="vvi" reg="rise" xml:id="ndor-001-a-2690">rise</w>
            <pc xml:id="ndor-001-a-2700">,</pc>
          </l>
          <l xml:id="ndor-e580">
            <w lemma="by" pos="acp-p" reg="By" xml:id="ndor-001-a-2710">By</w>
            <w lemma="equivocate" pos="j_vg" reg="Equivocating" xml:id="ndor-001-a-2720">Equiuocating</w>
            <w lemma="and" pos="cc" reg="and" xml:id="ndor-001-a-2730">and</w>
            <w lemma="his" pos="po" reg="his" xml:id="ndor-001-a-2740">his</w>
            <w lemma="cheverill" pos="n1" reg="Cheverill" xml:id="ndor-001-a-2750">Cheuerill</w>
            <w lemma="lie" pos="vvz" reg="lies" xml:id="ndor-001-a-2760">lyes</w>
            <pc xml:id="ndor-001-a-2770">:</pc>
          </l>
          <l xml:id="ndor-e590">
            <w lemma="and" pos="cc" reg="And" xml:id="ndor-001-a-2780">And</w>
            <w lemma="truth" pos="n1" reg="Truth" xml:id="ndor-001-a-2790" rendition="#hi">Truth</w>
            
            <w lemma="to" pos="acp-p" reg="to" xml:id="ndor-001-a-2800">to</w>
            <w lemma="all" pos="d" reg="all" xml:id="ndor-001-a-2810">all</w>
            <w lemma="appeal" pos="n2" reg="appeals" xml:id="ndor-001-a-2820">appeales</w>
            <w lemma="to" pos="acp-p" reg="to" xml:id="ndor-001-a-2830">to</w>
            <w lemma="open" pos="j" reg="open" xml:id="ndor-001-a-2840">open</w>
            <w lemma="view" pos="n1" reg="view" xml:id="ndor-001-a-2850">view</w>
            <pc xml:id="ndor-001-a-2860">,</pc>
          </l>
          <l xml:id="ndor-e610">
            <w lemma="bid" pos="vvg" reg="Bidding" xml:id="ndor-001-a-2870">Bidding</w>
            <w lemma="all" pos="d" reg="all" xml:id="ndor-001-a-2880">all</w>
            <w lemma="heresy" pos="n2" reg="Heresies" xml:id="ndor-001-a-2890">Heresies</w>
            <w lemma="for" pos="acp-p" reg="for" xml:id="ndor-001-a-2900">for</w>
            <w lemma="ere" pos="acp-p" reg="ere" xml:id="ndor-001-a-2910">ere</w>
            <w lemma="adieu" pos="uh" reg="adieu" xml:id="ndor-001-a-2920">adew</w>
          </l>
          <l xml:id="ndor-e620">
            <w lemma="desire" pos="vvg" reg="Desiring" xml:id="ndor-001-a-2930">Desiring</w>
            <w lemma="our" pos="po" reg="our" xml:id="ndor-001-a-2940">our</w>
            <w lemma="great" pos="j" reg="great" xml:id="ndor-001-a-2950">great</w>
            <w lemma="CHARLES" pos="n1-nn" reg="CHARLES" xml:id="ndor-001-a-2960">CHARLES</w>
            <w lemma="to" pos="acp-cs" reg="to" xml:id="ndor-001-a-2970">to</w>
            <w lemma="take" pos="vvi" reg="take" xml:id="ndor-001-a-2980">take</w>
            <w lemma="to" pos="acp-p" reg="to" xml:id="ndor-001-a-2990">to</w>
            <w lemma="heart" pos="n1" reg="heart" xml:id="ndor-001-a-3000">heart</w>
            <w lemma="▪" pos="▪" reg="▪" xml:id="ndor-001-a-3010">▪</w>
          </l>
          <l xml:id="ndor-e630">
            <w lemma="and" pos="cc" reg="And" xml:id="ndor-001-a-3020">And</w>
            <w lemma="by" pos="acp-p" reg="by" xml:id="ndor-001-a-3030">by</w>
            <w lemma="the" pos="d" reg="the" xml:id="ndor-001-a-3040">the</w>
            <w lemma="parliament" pos="n1" reg="Parliament" xml:id="ndor-001-a-3050">Parlament</w>
            <w lemma="make" pos="vvb" reg="make" xml:id="ndor-001-a-3060">make</w>
            <w lemma="armenius" pos="n1-nn" reg="Armenius" xml:id="ndor-001-a-3070" rendition="#hi">Armenius</w>
            
            <w lemma="smart" pos="n1" reg="smart" xml:id="ndor-001-a-3080">smart</w>
            <pc unit="sentence" xml:id="ndor-001-a-3090">.</pc>
          </l>
          <l xml:id="ndor-e650">
            <w lemma="which" pos="crq-r" reg="Which" xml:id="ndor-001-a-3100">Which</w>
            <w lemma="be" pos="vvg" reg="being" xml:id="ndor-001-a-3110">being</w>
            <w lemma="do" pos="vvn" reg="done" xml:id="ndor-001-a-3120">done</w>
            <w lemma="England" pos="n1-nn" reg="England" xml:id="ndor-001-a-3130" rendition="#hi">England</w>
            
            <w lemma="shall" pos="vmb" reg="shall" xml:id="ndor-001-a-3140">shall</w>
            <w lemma="ever" pos="av" reg="ever" xml:id="ndor-001-a-3150">euer</w>
            <w lemma="bless" pos="vvi" reg="bless" xml:id="ndor-001-a-3160">blesse</w>
          </l>
          <l xml:id="ndor-e670">
            <w lemma="the" pos="d" reg="The" xml:id="ndor-001-a-3170">The</w>
            <w lemma="king" pos="n1" reg="King" xml:id="ndor-001-a-3180" rendition="#hi">King</w><pc xml:id="ndor-001-a-3190">,</pc>
            
              
            <w lemma="the" pos="d" reg="the" xml:id="ndor-001-a-3200">the</w>
            <w lemma="state" pos="n1" reg="State" xml:id="ndor-001-a-3210" rendition="#hi">State</w><pc xml:id="ndor-001-a-3220">,</pc>
            
              
            <w lemma="the" pos="d" reg="the" xml:id="ndor-001-a-3230">the</w>
            <w lemma="church" pos="n1g" reg="Church's" xml:id="ndor-001-a-3240" rendition="#hi">Churches</w>
            
            <w lemma="happiness" pos="n1" reg="happiness" xml:id="ndor-001-a-3250">happinesse</w>
            <pc xml:id="ndor-001-a-3260">,</pc>
          </l>
          <l xml:id="ndor-e710">
            <w lemma="and" pos="cc" reg="And" xml:id="ndor-001-a-3270">And</w>
            <w lemma="if" pos="cs" reg="if" xml:id="ndor-001-a-3280">if</w>
            <w lemma="for" pos="acp-p" reg="for" xml:id="ndor-001-a-3290">for</w>
            <w lemma="tell" pos="vvg" reg="telling" xml:id="ndor-001-a-3300">telling</w>
            <w lemma="truth" pos="n1" reg="truth" xml:id="ndor-001-a-3310">truth</w>
            <pc xml:id="ndor-001-a-3320">,</pc>
            <w lemma="i" pos="pns" reg="I" xml:id="ndor-001-a-3330">I</w>
            <w lemma="burn" pos="vvb" reg="burn" xml:id="ndor-001-a-3340">burne</w>
            <w lemma="or" pos="cc" reg="or" xml:id="ndor-001-a-3350">or</w>
            <w lemma="fry" pos="vvi" reg="fry" xml:id="ndor-001-a-3360">frye</w>
            <pc xml:id="ndor-001-a-3370">;</pc>
          </l>
          <l xml:id="ndor-e720">
            <w lemma="what" pos="crq-q" reg="What" xml:id="ndor-001-a-3380">What</w>
            <w lemma="then" pos="av" reg="than" xml:id="ndor-001-a-3390">then</w>
            <w lemma="deserve" pos="vvz" reg="deserves" xml:id="ndor-001-a-3400">deserues</w>
            <w lemma="he" pos="pns" reg="he" xml:id="ndor-001-a-3410">he</w>
            <w lemma="that" pos="cs" reg="that" xml:id="ndor-001-a-3420">that</w>
            <w lemma="tell" pos="vvz" reg="tells" xml:id="ndor-001-a-3430">tels</w>
            <w lemma="a" pos="d" reg="a" xml:id="ndor-001-a-3440">a</w>
            <w lemma="lie" pos="n1" reg="lie" xml:id="ndor-001-a-3450">lye</w>
            <pc unit="sentence" xml:id="ndor-001-a-3460">?</pc>
            <w lemma="1628." pos="crd" reg="1628." xml:id="ndor-001-a-3470">1628.</w>
            <pc unit="sentence" xml:id="ndor-001-a-3480"/>
          </l>
        </lg>
        <trailer xml:id="ndor-e730">
          <w lemma="finis" pos="fw-la" reg="FINIS" xml:id="ndor-001-a-3490">FINIS</w>
          <pc unit="sentence" xml:id="ndor-001-a-3500">.</pc>
        </trailer>
      </div>
    </body>
    <back xml:id="ndor-t-b"/>
  </text>
</TEI>

